 Device is  TITAN X (Pascal)  GPU from  NVIDIA Corporation  with a max of 28 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 145524 microseconds.
   (= 145524 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          206558.7     0.015504     0.015492     0.015515
Scale:         206546.0     0.015497     0.015493     0.015511
Add:           206733.7     0.015482     0.015479     0.015492
Triad:         206810.2     0.015476     0.015473     0.015483
-------------------------------------------------------------
time is 0.069915 sec
pmax = 2952703.053329
-------------------------------------------------------------
grayscale time is 0.578953 sec
grayscale Gflops is 1.253573 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.096477 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 40.715849
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 1.467926 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.001780 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.001620 seconds
-------------------------------------------------------------
Average runtime: 0.490442  GFLOPS: 110104.754205
 
 Device is  TITAN X (Pascal)  GPU from  NVIDIA Corporation  with a max of 28 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 147096 microseconds.
   (= 147096 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          206584.2     0.015502     0.015490     0.015519
Scale:         206663.7     0.015489     0.015484     0.015498
Add:           206813.4     0.015476     0.015473     0.015486
Triad:         206822.9     0.015475     0.015472     0.015484
-------------------------------------------------------------
time is 10.712226 sec
pmax = 19271.288438
-------------------------------------------------------------
grayscale time is 0.599235 sec
grayscale Gflops is 1.211144 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.106103 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 40.795668
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 1.432020 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.001617 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.001608 seconds
-------------------------------------------------------------
Average runtime: 0.478415  GFLOPS: 112872.712195
