// fermi



__kernel void testKernel(const int groups, const int wsize, const int niters, const float step_size, __global float* partialsum, __local float* localsum) {
    const int i = get_group_id(0);
    const int wj = get_local_id(1);
    const int tj = get_local_id(0);

    const int nrThreadsWsize = 32;
    const int nrWarpsWsize = wsize == 1 * nrThreadsWsize ?
        1 :
        wsize % (1 * nrThreadsWsize) == 0 ?
            wsize / (1 * nrThreadsWsize) :
            wsize / (1 * nrThreadsWsize) + 1
    ;
   // __local float localsum[wsize];
    const int j = wj * (1 * nrThreadsWsize) + tj;
    if (j < wsize) {
        const int istart = (i * wsize + j) * niters;
        const int iend = istart + niters;
        float x = 0.0;
        float accum = 0.0;
        for (int a = istart; a < iend; a++) {
            x = (a + 0.5) * step_size;
            accum += 4.0 / (1.0 + x * x);
        }
        localsum[j] = accum;
        barrier(CLK_LOCAL_MEM_FENCE);
        if (tj == 0) {
            float sum = 0.0;
            for (int b = 0; b < wsize; b++) {
                sum += localsum[b];
            }
            partialsum[i] = sum;
        }
    }
}


