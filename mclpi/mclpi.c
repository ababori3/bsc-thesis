#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <sys/time.h>
#include<string.h>
#include <sys/types.h>
#include <CL/opencl.h>
#include"err_code.h"

cl_device_id device_id; // compute device id
cl_context context; // compute context
cl_command_queue commands; // compute command queue
cl_program program; // compute program
cl_kernel kernel_pi; // compute kernel
cl_mem d_partial_sums;

cl_mem d_a;
cl_mem d_c;

static FILE *fp;
static char *kernelSource;
static size_t source_size;

long long INSTEPS = (512*512*512);
#define ITERS (512)

#define ASIZE 5120

int a[ASIZE];
int c[ASIZE];
size_t workr = ASIZE;
size_t local_work = 512;

#define DEVICE CL_DEVICE_TYPE_GPU

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

double mysecond() {
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp, &tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6);
}

int output_device_info(cl_device_id device_id) {

    int err; // error code returned from OpenCL calls
    cl_device_type device_type; // Parameter defining the type of the compute device
    cl_uint comp_units; // the max number of compute units on a device
    cl_char vendor_name[1024] = {0}; // string to hold vendor name for compute device
    cl_char device_name[1024] = {0}; // string to hold name of compute device
#ifdef VERBOSE
    cl_uint max_work_itm_dims;
    size_t max_wrkgrp_size;
    size_t *max_loc_size;
#endif


    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof (device_name), &device_name, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device name!\n");
        return EXIT_FAILURE;
    }
    printf(" \n Device is  %s ", device_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof (device_type), &device_type, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device type information!\n");
        return EXIT_FAILURE;
    }
    if (device_type == CL_DEVICE_TYPE_GPU)
        printf(" GPU from ");

    else if (device_type == CL_DEVICE_TYPE_CPU)
        printf("\n CPU from ");

    else
        printf("\n non  CPU or GPU processor from ");

    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof (vendor_name), &vendor_name, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device vendor name!\n");
        return EXIT_FAILURE;
    }
    printf(" %s ", vendor_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (cl_uint), &comp_units, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device number of compute units !\n");
        return EXIT_FAILURE;
    }
    printf(" with a max of %d compute units \n", comp_units);

#ifdef VERBOSE
    //
    // Optionally print information about work group sizes
    //
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint),
            &max_work_itm_dims, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n",
                err_code(err));
        return EXIT_FAILURE;
    }

    max_loc_size = (size_t*) malloc(max_work_itm_dims * sizeof (size_t));
    if (max_loc_size == NULL) {
        printf(" malloc failed\n");
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims * sizeof (size_t),
            max_loc_size, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n", err_code(err));
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (size_t),
            &max_wrkgrp_size, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n", err_code(err));
        return EXIT_FAILURE;
    }
    printf("work group, work item information");
    printf("\n max loc dim ");
    for (int i = 0; i < max_work_itm_dims; i++)
        printf(" %d ", (int) (*(max_loc_size + i)));
    printf("\n");
    printf(" Max work group size = %d\n", (int) max_wrkgrp_size);
#endif

    return CL_SUCCESS;

}

int setup_cl_context() {

    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }
#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    //    if (device_id == NULL)
    //        checkError(err, "Getting device");

    err = output_device_info(device_id);
    //    checkError(err, "Outputting device info");

    // Create a compute context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    //    checkError(err, "Creating context");

    // Create a command queue
    commands = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    //    checkError(err, "Creating command queue");

    // Load the kernel source code into the array source_str

    FILE *file = fopen("/home/arian/mcl/output/mclPi.cl", "r");
    if (!file) {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *kernelSource = (char *) calloc(sizeof (char), len);
    if (!kernelSource) {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    fread(kernelSource, sizeof (char), len, file);
    fclose(file);

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource, NULL, &err);
    checkError(err, "Creating program");

    // Build the program  
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof (buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }



}

//int test_pi_mcl() {
//
//    int err;
//    float *h_psum; // vector to hold partial sum
//    int in_nsteps = INSTEPS; // default number of steps (updated later to device preferable)
//    int niters = ITERS; // number of iterations
//    int nsteps;
//    float step_size;
//    size_t nwork_groups;
//    size_t max_size, work_group_size = 8;
//    float pi_res;
//
//    kernel_pi = clCreateKernel(program, "testKernel", &err);
//    checkError(err, "Creating kernel");
//
//    // Find kernel work-group size
//    err = clGetKernelWorkGroupInfo(kernel_pi, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof (size_t), &work_group_size, NULL);
//    checkError(err, "Getting kernel work group info");
//    // Now that we know the size of the work-groups, we can set the number of
//    // work-groups, the actual number of steps, and the step size
//    nwork_groups = in_nsteps / (work_group_size * niters);
//
//    if (nwork_groups < 1) {
//        err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (size_t), &nwork_groups, NULL);
//        checkError(err, "Getting device compute unit info");
//        work_group_size = in_nsteps / (nwork_groups * niters);
//    }
//
//    nsteps = work_group_size * niters * nwork_groups;
//    step_size = 1.0f / (float) nsteps;
//    h_psum = calloc(sizeof (float), work_group_size);
//    if (!h_psum) {
//        printf("Error: could not allocate host memory for h_psum\n");
//        return EXIT_FAILURE;
//    }
//
//    printf(" %ld work-groups of size %ld. %d Integration steps\n",
//            nwork_groups,
//            work_group_size,
//            nsteps);
////    cl_mem local_sum = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof (float) * work_group_size * nwork_groups, NULL, &err);
//    d_partial_sums = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof (float) * nwork_groups * work_group_size, NULL, &err);
//    checkError(err, "Creating buffer d_partial_sums");
//    
//    
//    int wok = work_group_size * nwork_groups;
//    // Set kernel arguments
//    err |= clSetKernelArg(kernel_pi, 0, sizeof (int), &work_group_size);
////    err |= clSetKernelArg(kernel_pi, 1, sizeof (int), &nwork_groups);
//    err |= clSetKernelArg(kernel_pi, 1, sizeof (int), &niters);
//    err |= clSetKernelArg(kernel_pi, 2, sizeof (float), &step_size);
//    err |= clSetKernelArg(kernel_pi, 3, sizeof (cl_mem), &d_partial_sums);
////    err |= clSetKernelArg(kernel_pi, 5, sizeof (int), &wok);
////    err |= clSetKernelArg(kernel_pi, 5, sizeof (cl_mem), &d_partial_sums);
//    checkError(err, "Setting kernel args");
//
//    // Execute the kernel over the entire range of our 1D input data set
//    // using the maximum number of work items for this device
//    size_t global = nsteps / niters;
//    size_t local = work_group_size;
//    double rtime = mysecond();
//    err = clEnqueueNDRangeKernel(
//            commands,
//            kernel_pi,
//            1, NULL,
//            &global,
//            &local,
//            0, NULL, NULL);
//    checkError(err, "Enqueueing kernel");
//
//    err = clEnqueueReadBuffer(
//            commands,
//            d_partial_sums,
//            CL_TRUE,
//            0,
//            sizeof (float) * work_group_size,
//            h_psum,
//            0, NULL, NULL);
//    checkError(err, "Reading back d_partial_sums");
//
//    // complete the sum and compute the final integral value on the host
//    pi_res = 0.0f;
//    for (unsigned int i = 0; i < nwork_groups * work_group_size; i++) {
//        pi_res += h_psum[i];
//        printf("%f ", h_psum[i]);
//    }
//  pi_res *= step_size;
//
//    rtime = mysecond() - rtime;
//
//    printf("\nThe calculation ran in %11.6f seconds\n", rtime);
//    printf(" pi = %f for %d steps\n", pi_res, nsteps);
//}


//void test_kernel() {
//
//    int err;
//    memset(a, 0, ASIZE * 4);
//    memset(c, 0, ASIZE * 4);
//
//    kernel_pi = clCreateKernel(program, "test1", &err);
//    checkError(err, "Creating kernel");
//
//    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ASIZE * 4, a, &err);
//    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ASIZE * 4, c, &err);
//    err |= clSetKernelArg(kernel_pi, 0, sizeof (cl_mem), &d_a);
////    err |= clSetKernelArg(kernel_pi, 1, sizeof (cl_mem), &d_c);
//    err = clEnqueueNDRangeKernel(commands, kernel_pi, 1, NULL, &workr, &local_work, 0, NULL, NULL);
//    //                clFinish(commands);
//    err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, ASIZE * 4, a, 0, NULL, NULL);
//
//    for (int i = 0; i < ASIZE; i++) {
//        printf("%d ", a[i]);
//    }
//
//
//}

int test_pi_mcl() {

    int err;
    float *h_psum; // vector to hold partial sum
    int in_nsteps = INSTEPS; // default number of steps (updated later to device preferable)
    int niters = ITERS; // number of iterations
    int nsteps;
    float step_size;
    size_t nwork_groups;
    size_t max_size, work_group_size = 8;
    float pi_res;

    kernel_pi = clCreateKernel(program, "testKernel", &err);
    checkError(err, "Creating kernel");

    // Find kernel work-group size
    err = clGetKernelWorkGroupInfo(kernel_pi, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof (size_t), &work_group_size, NULL);
    checkError(err, "Getting kernel work group info");
    // Now that we know the size of the work-groups, we can set the number of
    // work-groups, the actual number of steps, and the step size
    nwork_groups = in_nsteps / (work_group_size * niters);

    if (nwork_groups < 1) {
        err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (size_t), &nwork_groups, NULL);
        checkError(err, "Getting device compute unit info");
        work_group_size = in_nsteps / (nwork_groups * niters);
    }

    nsteps = work_group_size * niters * nwork_groups;
    step_size = 1.0f / (float) nsteps;
    h_psum = calloc(sizeof (float), nwork_groups);
    if (!h_psum) {
        printf("Error: could not allocate host memory for h_psum\n");
        return EXIT_FAILURE;
    }

    printf(" %ld work-groups of size %ld. %d Integration steps\n",
            nwork_groups,
            work_group_size,
            nsteps);

    d_partial_sums = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof (float) * nwork_groups, NULL, &err);
    checkError(err, "Creating buffer d_partial_sums");

    // Set kernel arguments
    err |= clSetKernelArg(kernel_pi, 0, sizeof (int), &nwork_groups);
    err |= clSetKernelArg(kernel_pi, 1, sizeof (int), &work_group_size);
    err = clSetKernelArg(kernel_pi, 2, sizeof (int), &niters);
    err |= clSetKernelArg(kernel_pi, 3, sizeof (float), &step_size);
    err |= clSetKernelArg(kernel_pi, 5, sizeof (float) * work_group_size, NULL);
    err |= clSetKernelArg(kernel_pi, 4, sizeof (cl_mem), &d_partial_sums);
    checkError(err, "Settin kernel args");

    // Execute the kernel over the entire range of our 1D input data set
    // using the maximum number of work items for this device
    size_t global = nsteps / niters;
    size_t local = work_group_size;
    double rtime = mysecond();
    err = clEnqueueNDRangeKernel(
            commands,
            kernel_pi,
            1, NULL,
            &global,
            &local,
            0, NULL, NULL);
    checkError(err, "Enqueueing kernel");

    err = clEnqueueReadBuffer(
            commands,
            d_partial_sums,
            CL_TRUE,
            0,
            sizeof (float) * nwork_groups,
            h_psum,
            0, NULL, NULL);
    checkError(err, "Reading back d_partial_sums");

    // complete the sum and compute the final integral value on the host
    pi_res = 0.0f;
    for (unsigned int i = 0; i < nwork_groups; i++) {
        pi_res += h_psum[i];
    }
    pi_res *= step_size;

    rtime = mysecond() - rtime;

    printf("\nThe calculation ran in %11.6f seconds\n", rtime);
    printf(" pi = %f for %d steps\n", pi_res, nsteps);
}

int main(int argc, char** argv) {

    setup_cl_context();

    test_pi_mcl();
    return (EXIT_SUCCESS);
}

