#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <sys/time.h>
#include<string.h>


#include <sys/types.h>
#include <CL/opencl.h>

#include"err_code.h"

#define SHELLSCRIPT "/bin/bash -c mcl gen-cashmere -t fermi -- ~/Desktop/smcl/mclkernels/StreamScale.mcl"



#ifndef STREAM_ARRAY_SIZE
#define STREAM_ARRAY_SIZE 9000000
#endif

#ifdef NTIMES
#if NTIMES<=1
#define NTIMES 10
#endif
#endif
#ifndef NTIMES
#define NTIMES 10
#endif 


#ifndef OFFSET
#define OFFSET 0
#endif

#define HLINE "-------------------------------------------------------------\n"

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#ifndef STREAM_TYPE
#define STREAM_TYPE double
#endif

#define M 20
#define MAX_SOURCE_SIZE 25000
//#define VERBOSE

static STREAM_TYPE a[STREAM_ARRAY_SIZE + OFFSET],
b[STREAM_ARRAY_SIZE + OFFSET],
c[STREAM_ARRAY_SIZE + OFFSET];

static double avgtime[4] = {0}, maxtime[4] = {0},
mintime[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

static char *label[4] = {"Copy:      ", "Scale:     ",
    "Add:       ", "Triad:     "};

static double bytes[4] = {
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    3 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    3 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE
};

int quantum, checktick();
int BytesPerWord;
int k;
ssize_t j;
STREAM_TYPE scalar;
double t, times[4][NTIMES];
size_t workr = (size_t) (STREAM_ARRAY_SIZE * 4);
size_t local_work = 512;

cl_device_id device_id; // compute device id
cl_context context; // compute context
cl_command_queue commands; // compute command queue
cl_program program; // compute program
cl_kernel ko_test; // compute kernel
cl_kernel ko_copy; // compute kernel
cl_kernel ko_scalar; // compute kernel
cl_kernel ko_add; // compute kernel
cl_kernel ko_triad; // compute kernel
cl_mem d_a;
cl_mem d_b;
cl_mem d_c;

FILE *fp;
char *kernelSource;
size_t source_size;

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif


extern double mysecond();
extern void checkSTREAMresults();

double mysecond() {
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp, &tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6);
}

void print_results() {
    for (k = 1; k < NTIMES; k++) /* note -- skip first iteration */ {
        for (j = 0; j < 4; j++) {
            avgtime[j] = avgtime[j] + times[j][k];
            mintime[j] = MIN(mintime[j], times[j][k]);
            maxtime[j] = MAX(maxtime[j], times[j][k]);
        }
    }

    printf("Function    Best Rate MB/s  Avg time     Min time     Max time\n");
    for (j = 0; j < 4; j++) {
        avgtime[j] = avgtime[j] / (double) (NTIMES - 1);

        printf("%s%12.1f  %11.6f  %11.6f  %11.6f\n", label[j],
                1.0E-06 * bytes[j] / mintime[j],
                avgtime[j],
                mintime[j],
                maxtime[j]);
    }
    printf(HLINE);
}

void test_stream_cl() {
    int err = 0;
    scalar = 3;
    ko_copy = clCreateKernel(program, "copy_kernel", &err);
    checkError(err, "Creating kernel");

    ko_scalar = clCreateKernel(program, "scale_kernel", &err);
    checkError(err, "Creating kernel");

    ko_add = clCreateKernel(program, "add_kernel", &err);
    checkError(err, "Creating kernel");

    ko_triad = clCreateKernel(program, "triad_kernel", &err);
    checkError(err, "Creating kernel");


    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, b, &err);
    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, c, &err);


    for (k = 0; k < NTIMES; k++) {

        times[0][k] = mysecond();
        err = clSetKernelArg(ko_copy, 0, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_copy, 1, sizeof (cl_mem), &d_c);
        err = clEnqueueNDRangeKernel(commands, ko_copy, 1, NULL, &workr, &local_work, 0, NULL, NULL);
        //        clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[0][k] = mysecond() - times[0][k];

        times[1][k] = mysecond();
        err = clSetKernelArg(ko_scalar, 0, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_scalar, 1, sizeof (cl_mem), &d_c);
        //        err = clSetKernelArg(ko_scalar, 2, sizeof (double), &scalar);
        err = clEnqueueNDRangeKernel(commands, ko_scalar, 1, NULL, &workr, &local_work, 0, NULL, NULL);
        //                clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[1][k] = mysecond() - times[1][k];
        //
        times[2][k] = mysecond();
        err = clSetKernelArg(ko_add, 0, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_add, 1, sizeof (cl_mem), &d_b);
        err = clSetKernelArg(ko_add, 2, sizeof (cl_mem), &d_c);
        err = clEnqueueNDRangeKernel(commands, ko_add, 1, NULL, &workr, &local_work, 0, NULL, NULL);
        //        clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[2][k] = mysecond() - times[2][k];

        times[3][k] = mysecond();
        err = clSetKernelArg(ko_triad, 0, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_triad, 1, sizeof (cl_mem), &d_b);
        err = clSetKernelArg(ko_triad, 2, sizeof (cl_mem), &d_c);
        err = clSetKernelArg(ko_triad, 3, sizeof (double), &scalar);
        err = clEnqueueNDRangeKernel(commands, ko_copy, 1, NULL, &workr, &local_work, 0, NULL, NULL);
        //        clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, a, 0, NULL, NULL);
        times[3][k] = mysecond() - times[3][k];


    }



}

void get_test_times(double t, int quantum) {
    int err = 0;
    size_t workr = (size_t) (STREAM_ARRAY_SIZE * 4);
    size_t local_work = 1;
    t = mysecond();

    //    for (int j = 0; j < STREAM_ARRAY_SIZE; j++) a[j] = 2.0E0 * a[j];

    // Create the compute kernel from the program 
    ko_test = clCreateKernel(program, "test_kernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    err = clSetKernelArg(ko_test, 0, sizeof (cl_mem), &d_a);
    err = clEnqueueNDRangeKernel(commands, ko_test, 1, NULL, &workr, &local_work, 0, NULL, NULL);
    err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, a, 0, NULL, NULL);
    checkError(err, "Enqueueing kernel 1st time");
    t = 1.0E6 * (mysecond() - t);

    printf("Each test below will take on the order"
            " of %d microseconds.\n", (int) t);
    printf("   (= %d clock ticks)\n", (int) (t / quantum));
    printf("Increase the size of the arrays if this shows that\n");
    printf("you are not getting at least 20 clock ticks per test.\n");

    printf(HLINE);

    printf("WARNING -- The above is only a rough guideline.\n");
    printf("For best results, please be sure you know the\n");
    printf("precision of your system timer.\n");
    printf(HLINE);
}

int checktick() {
    int i, minDelta, Delta;
    double t1, t2, timesfound[M];

    /*  Collect a sequence of M unique time values from the system. */

    for (i = 0; i < M; i++) {
        t1 = mysecond();
        while (((t2 = mysecond()) - t1) < 1.0E-6)
            ;
        timesfound[i] = t1 = t2;
    }

    /*
     * Determine the minimum difference between these M values.
     * This result will be our estimate (in microseconds) for the
     * clock granularity.
     */

    minDelta = 1000000;
    for (i = 1; i < M; i++) {
        Delta = (int) (1.0E6 * (timesfound[i] - timesfound[i - 1]));
        minDelta = MIN(minDelta, MAX(Delta, 0));
    }

    return (minDelta);
}

int check_granularity(int j, int quantum) {

    for (j = 0; j < STREAM_ARRAY_SIZE; j++) {
        a[j] = 1.0;
        b[j] = 2.0;
        c[j] = 0.0;
    }

    printf(HLINE);

    if ((quantum = checktick()) >= 1) {
        printf("Your clock granularity/precision appears to be " "%d microseconds.\n", quantum);
    } else {
        printf("Your clock granularity appears to be "
                "less than one microsecond.\n");
        quantum = 1;
    }
    return quantum;
}

int output_device_info(cl_device_id device_id) {
    int err; // error code returned from OpenCL calls
    cl_device_type device_type; // Parameter defining the type of the compute device
    cl_uint comp_units; // the max number of compute units on a device
    cl_char vendor_name[1024] = {0}; // string to hold vendor name for compute device
    cl_char device_name[1024] = {0}; // string to hold name of compute device
#ifdef VERBOSE
    cl_uint max_work_itm_dims;
    size_t max_wrkgrp_size;
    size_t *max_loc_size;
#endif


    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof (device_name), &device_name, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device name!\n");
        return EXIT_FAILURE;
    }
    printf(" \n Device is  %s ", device_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof (device_type), &device_type, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device type information!\n");
        return EXIT_FAILURE;
    }
    if (device_type == CL_DEVICE_TYPE_GPU)
        printf(" GPU from ");

    else if (device_type == CL_DEVICE_TYPE_CPU)
        printf("\n CPU from ");

    else
        printf("\n non  CPU or GPU processor from ");

    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof (vendor_name), &vendor_name, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device vendor name!\n");
        return EXIT_FAILURE;
    }
    printf(" %s ", vendor_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (cl_uint), &comp_units, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device number of compute units !\n");
        return EXIT_FAILURE;
    }
    printf(" with a max of %d compute units \n", comp_units);

#ifdef VERBOSE
    //
    // Optionally print information about work group sizes
    //
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint),
            &max_work_itm_dims, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n",
                err_code(err));
        return EXIT_FAILURE;
    }

    max_loc_size = (size_t*) malloc(max_work_itm_dims * sizeof (size_t));
    if (max_loc_size == NULL) {
        printf(" malloc failed\n");
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims * sizeof (size_t),
            max_loc_size, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n", err_code(err));
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (size_t),
            &max_wrkgrp_size, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n", err_code(err));
        return EXIT_FAILURE;
    }
    printf("work group, work item information");
    printf("\n max loc dim ");
    for (int i = 0; i < max_work_itm_dims; i++)
        printf(" %d ", (int) (*(max_loc_size + i)));
    printf("\n");
    printf(" Max work group size = %d\n", (int) max_wrkgrp_size);
#endif

    return CL_SUCCESS;

}

int setup_cl_context() {
    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    //    if (device_id == NULL)
    //        checkError(err, "Getting device");

    err = output_device_info(device_id);
    //    checkError(err, "Outputting device info");

    // Create a compute context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    //    checkError(err, "Creating context");

    // Create a command queue
    commands = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    //    checkError(err, "Creating command queue");

    // Load the kernel source code into the array source_str

    FILE *file = fopen("stream_cl_kernel.cl", "r");
    if (!file) {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *kernelSource = (char *) calloc(sizeof (char), len);
    if (!kernelSource) {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    fread(kernelSource, sizeof (char), len, file);
    fclose(file);

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource, NULL, &err);
    checkError(err, "Creating program");

    // Build the program  
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof (buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }



}

void intro() {
    /* --- SETUP --- determine precision and check timing --- */
    int BytesPerWord;

    printf(HLINE);
    printf("STREAM version $Revision: 5.10 $\n");
    printf(HLINE);
    BytesPerWord = sizeof (STREAM_TYPE);
    printf("This system uses %d bytes per array element.\n",
            BytesPerWord);

    printf(HLINE);
#ifdef N
    printf("*****  WARNING: ******\n");
    printf("      It appears that you set the preprocessor variable N when compiling this code.\n");
    printf("      This version of the code uses the preprocesor variable STREAM_ARRAY_SIZE to control the array size\n");
    printf("      Reverting to default value of STREAM_ARRAY_SIZE=%llu\n", (unsigned long long) STREAM_ARRAY_SIZE);
    printf("*****  WARNING: ******\n");
#endif

    printf("Array size = %llu (elements), Offset = %d (elements)\n", (unsigned long long) STREAM_ARRAY_SIZE, OFFSET);
    printf("Memory per array = %.1f MiB (= %.1f GiB).\n",
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0),
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0 / 1024.0));
    printf("Total memory required = %.1f MiB (= %.1f GiB).\n",
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.),
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024. / 1024.));
    printf("Each kernel will be executed %d times.\n", NTIMES);
    printf(" The *best* time for each kernel (excluding the first iteration)\n");
    printf(" will be used to compute the reported bandwidth.\n");
}

void test_kernel() {
    int err;
    size_t dataSize = 1600;
    size_t bla = 1;
    //    float* h_a = (float *) malloc(40);
    //    float* h_b = (float *) malloc(40);
    float h_a[400];
    float h_b[400];
    memset(h_a, 0, 400 * sizeof (int));
    memset(h_b, 0, 400 * sizeof (int));
    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, dataSize, h_a, &err);
    err = clSetKernelArg(ko_copy, 0, sizeof (cl_mem), &d_a);
    err = clEnqueueNDRangeKernel(commands, ko_copy, 1, NULL, &dataSize, &bla, 0, NULL, NULL);
    err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, 1600, h_a, 0, NULL, NULL);
    checkError(err, "Enqueueing kernel 1st time");
    for (int i = 0; i < 400; i++) {
        printf("%.1f ", h_a[i]);

    }
    //    free(h_a);
    //    free(h_b);
    //    checkError(err, "Creating buffer d_a");
}

int main(int argc, char** argv) {
//        intro();
//        setup_cl_context();
//        quantum = check_granularity(j, quantum);
//        get_test_times(t, quantum);
//        test_stream_cl();
//        print_results();

    system("whoami");
    



    return (EXIT_SUCCESS);
}

