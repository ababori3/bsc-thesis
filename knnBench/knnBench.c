
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <sys/time.h>
#include<string.h>
#include <sys/types.h>
#include <CL/opencl.h>
#include <stdio_ext.h>
#include <math.h>
#include "err_code.h"

#define VERBOSE

#ifndef STREAM_ARRAY_SIZE
#define STREAM_ARRAY_SIZE 25000000
#endif

#ifdef NTIMES
#if NTIMES<=1
#define NTIMES 10
#endif
#endif
#ifndef NTIMES
#define NTIMES 10
#endif 


#ifndef OFFSET
#define OFFSET 0
#endif
#define HLINE "-------------------------------------------------------------\n"

#ifndef STREAM_TYPE
#define STREAM_TYPE double
#endif

cl_uint comp_units; // the max number of compute units on a device
cl_device_type device_type; // Parameter defining the type of the compute device
cl_char vendor_name[1024] = {0}; // string to hold vendor name for compute device
cl_char device_name[1024] = {0}; // string to hold name of compute device
#ifdef VERBOSE
cl_uint max_work_itm_dims;
size_t max_wrkgrp_size;
size_t *max_loc_size;
#endif


cl_device_id device_id; // compute device id
cl_context context; // compute context
cl_command_queue commands; // compute command queue
cl_program program; // compute program

cl_kernel ko_nearest;
cl_kernel ko_bitonic;
cl_kernel ko_mode;

cl_mem d_a;
cl_mem d_b;
cl_mem d_c;

static FILE *fp;
static char *kernelSource;
static size_t source_size;

static FILE *bm_log;

#define DEVICE CL_DEVICE_TYPE_GPU

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

//knn

#ifndef KNNP
#define KNNP 8
#endif

#ifndef KNNK
#define KNNK 10
#endif

typedef struct Point {
    int x;
    int y;
    int class;

} Point;

typedef struct Dist_point {
    float dist;
    int class;

} Dist_point;


int goal_x = 200;
int goal_y = 75;
int goal_class = 0;

Point* field;
Point target;

Dist_point* distances;

size_t knnp = pow(2, KNNP);

void init_log() {
    //    fclose(fopen("bm_log.txt", "w"));
    bm_log = fopen("bm_log.txt", "a+");
    //    bm_log = fopen("bm_log.txt", "w");

}

int output_device_info(cl_device_id device_id) {

    int err; // error code returned from OpenCL calls

    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof (device_name), &device_name, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device name!\n");
        return EXIT_FAILURE;
    }
    fprintf(bm_log, " \n Device is  %s ", device_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof (device_type), &device_type, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device type information!\n");
        return EXIT_FAILURE;
    }
    if (device_type == CL_DEVICE_TYPE_GPU)
        fprintf(bm_log, " GPU from ");

    else if (device_type == CL_DEVICE_TYPE_CPU)
        fprintf(bm_log, "\n CPU from ");

    else
        fprintf(bm_log, "\n non  CPU or GPU processor from ");

    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof (vendor_name), &vendor_name, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device vendor name!\n");
        return EXIT_FAILURE;
    }
    fprintf(bm_log, " %s ", vendor_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (cl_uint), &comp_units, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device number of compute units !\n");
        return EXIT_FAILURE;
    }
    fprintf(bm_log, " with a max of %d compute units \n", comp_units);

#ifdef VERBOSE
    //
    // Optionally print information about work group sizes
    //
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint),
            &max_work_itm_dims, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n%s\n",
                err_code(err));
        return EXIT_FAILURE;
    }

    max_loc_size = (size_t*) malloc(max_work_itm_dims * sizeof (size_t));
    if (max_loc_size == NULL) {
        fprintf(bm_log, " malloc failed\n");
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims * sizeof (size_t),
            max_loc_size, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n%s\n", err_code(err));
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (size_t),
            &max_wrkgrp_size, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n%s\n", err_code(err));
        return EXIT_FAILURE;
    }
    fprintf(bm_log, "work group, work item information");
    fprintf(bm_log, "\n max loc dim ");
    for (int i = 0; i < max_work_itm_dims; i++)
        fprintf(bm_log, " %d ", (int) (*(max_loc_size + i)));
    fprintf(bm_log, "\n");
    fprintf(bm_log, " Max work group size = %d\n", (int) max_wrkgrp_size);
#endif

    return CL_SUCCESS;

}

void intro() {

    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        fprintf(bm_log, "Found 0 platforms!\n");
        exit(1);
    }
#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Getting device");

    err = output_device_info(device_id);

    /* --- SETUP --- determine precision and check timing --- */
    int BytesPerWord;

    BytesPerWord = sizeof (STREAM_TYPE);
    fprintf(bm_log, "This system uses %d bytes per array element.\n",
            BytesPerWord);

    fprintf(bm_log, HLINE);

    fprintf(bm_log, "Array size = %llu (elements), Offset = %d (elements)\n", (unsigned long long) STREAM_ARRAY_SIZE, OFFSET);
    fprintf(bm_log, "Memory per array = %.1f MiB (= %.1f GiB).\n",
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0),
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0 / 1024.0));
    fprintf(bm_log, "Total memory required = %.1f MiB (= %.1f GiB).\n",
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.),
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024. / 1024.));
    fprintf(bm_log, "Each kernel will be executed %d times.\n", NTIMES);
    fprintf(bm_log, " The *best* time for each kernel (excluding the first iteration)\n");
    fprintf(bm_log, " will be used to compute the reported bandwidth.\n");
}

double mysecond() {
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp, &tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6);
}

int setup_cl_context(char *input) {

    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        fprintf(bm_log, "Found 0 platforms!\n");
        return EXIT_FAILURE;
    }
#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    //    if (device_id == NULL)
    //        checkError(err, "Getting device");

    //    err = output_device_info(device_id);
    //    checkError(err, "Outputting device info");

    // Create a compute context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    //    checkError(err, "Creating context");

    // Create a command queue
    commands = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    //    checkError(err, "Creating command queue");

    // Load the kernel source code into the array source_str

    //    FILE *file = fopen("/home/arian/mcl/output/matrixmultiplication.cl", "r");
    FILE *file = fopen(input, "r");
    if (!file) {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *kernelSource = (char *) calloc(sizeof (char), len);
    if (!kernelSource) {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    fread(kernelSource, sizeof (char), len, file);
    fclose(file);

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource, NULL, &err);
    checkError(err, "Creating program");

    //free kernelSource
    free(kernelSource);

    // Build the program  
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        fprintf(bm_log, "Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof (buffer), buffer, &len);
        fprintf(bm_log, "%s\n", buffer);
        return EXIT_FAILURE;
    }

}

void generate_field() {

    size_t knnp = pow(2, KNNP);
    field = malloc(knnp * sizeof (Point));

    for (int i = 0; i < knnp; i++) {

        field[i].x = rand() % 255;
        field[i].y = rand() % 255;
        int clas = rand() % 9;
        field[i].class = (clas > 0) ? clas : 1;

    }

    //    for (int j = 0; j < knnp; j++) {
    //        printf("%d %d %d\n", field[j].x, field[j].y, field[j].class);
    //    }

}

void define_goal(int x_input, int y_input, int class_input) {

    //    goal = malloc(sizeof(Point));
    if (x_input > 200 || y_input > 200 || class_input > 8) {
        printf("incorrect size");
        exit(1);
    }

    target.x = x_input;
    target.y = y_input;
    target.class = class_input;

}

void get_euclidean() {

    size_t knnp = pow(2, KNNP);
    distances = malloc(knnp * sizeof (Dist_point));

    int err;
    double rtime;
    size_t gloc = MIN(max_wrkgrp_size, knnp);
    size_t workknn = knnp * sizeof (Point);

    ko_nearest = clCreateKernel(program, "distKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Point), field, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), distances, &err);

    err = clSetKernelArg(ko_nearest, 0, sizeof (int), &knnp);
    err = clSetKernelArg(ko_nearest, 1, sizeof (Point), &target);
    err = clSetKernelArg(ko_nearest, 2, sizeof (cl_mem), &d_a);
    err = clSetKernelArg(ko_nearest, 3, sizeof (cl_mem), &d_b);

    err = clEnqueueNDRangeKernel(commands, ko_nearest, 1, NULL, &workknn, &gloc, 0, NULL, NULL);
    err = clEnqueueReadBuffer(commands, d_b, CL_TRUE, 0, knnp * sizeof (Dist_point), distances, 0, NULL, NULL);

    clReleaseKernel(ko_nearest);
    //    printf("DISTANCES------------------\n\n");
    //
    //    for (int j = 0; j < knnp; j++) {
    //        printf("%f %d \n", distances[j].dist, distances[j].class);
    //    }


}

void get_bitonic() {

    size_t knnp = pow(2, KNNP);
    size_t stages = log(knnp) / log(2);
    size_t direction = 1;
    //        Dist_point* input = malloc(knnp * sizeof (Dist_point));
    //        for (int i = 0; i < 5; i++) {
    //            input[i].dist = 2;
    //            input[i].class = rand() % 8;
    //        }
    //    
    //        for (int i = 5; i < knnp; i++) {
    //            input[i].dist = rand() % 225;
    //            input[i].class = rand() % 8;
    //        }
    //
    //
    //
    //        for (int i = 0; i < knnp; i++) {
    //            printf("%f ", input[i].dist);
    //        }

    int err;
    double rtime;

    size_t gloc = MIN(max_wrkgrp_size, knnp);

    ko_bitonic = clCreateKernel(program, "bitonicKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), distances, &err);
    //     d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), input, &err);

    err = clSetKernelArg(ko_bitonic, 0, sizeof (int), &knnp);
    err = clSetKernelArg(ko_bitonic, 1, sizeof (cl_mem), &d_a);
    err = clSetKernelArg(ko_bitonic, 4, sizeof (uint), &direction);


    for (cl_uint stage = 0; stage < stages; ++stage) {
        err = clSetKernelArg(ko_bitonic, 2, sizeof (cl_uint), &stage);

        for (cl_uint subStage = 0; subStage < stage + 1; subStage++) {
            err = clSetKernelArg(ko_bitonic, 3, sizeof (cl_uint), &subStage);
            err = clEnqueueNDRangeKernel(commands, ko_bitonic, 1, NULL, &knnp, &gloc, 0, NULL, NULL);
            err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, knnp * sizeof (Dist_point), distances, 0, NULL, NULL);
            //            err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, knnp * sizeof (Dist_point), input, 0, NULL, NULL);



        }

    }

    clReleaseKernel(ko_bitonic);
    //    printf("sorted-----------------\n ");
    //    for (int i = 0; i < knnp; i++) {
    //        printf("%f %d\n", distances[i].dist, distances[i].class);
    //    }

    //        for (int i = 0; i < knnp; i++) {
    //        printf("%f %d\n", input[i].dist, input[i].class);
    //    }



}

void get_nearestk() {

    int err;
    double rtime;
    size_t gloc = MIN(max_wrkgrp_size, knnp);
    size_t workknn = knnp * sizeof (Dist_point);
    size_t k = KNNK;
    int count;
    int maxCount = 0;
    int maxValue = 0;

    ko_mode = clCreateKernel(program, "modeKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), distances, &err);


    for (int i = 1; i <= 8; i++) {

        count = 0;
        d_b = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof (int), &count, &err);

        err = clSetKernelArg(ko_mode, 0, sizeof (size_t), &knnp);
        err = clSetKernelArg(ko_mode, 1, sizeof (size_t), &k);
        err = clSetKernelArg(ko_mode, 2, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_mode, 3, sizeof (cl_mem), &d_b);
        err = clSetKernelArg(ko_mode, 4, sizeof (int), &i);
        err = clEnqueueNDRangeKernel(commands, ko_mode, 1, NULL, &workknn, &gloc, 0, NULL, NULL);
        err = clEnqueueReadBuffer(commands, d_b, CL_TRUE, 0, sizeof (int), &count, 0, NULL, NULL);

        if (count > maxCount) {
            maxCount = count;
            maxValue = i;
        }

    }

    clReleaseKernel(ko_mode);
    printf("class = %d with amount %d\n", maxValue, maxCount);


}

void mcl_main() {

    generate_field();
    define_goal(goal_x, goal_y, goal_class);

    setup_cl_context("./oclkernels/knn/dist.cl");
    get_euclidean();

    setup_cl_context("./oclkernels/knn/bitonic.cl");
    get_bitonic();
    setup_cl_context("./oclkernels/knn/mode.cl");
    get_nearestk();
}

int main(int argc, char** argv) {

    init_log();
    intro();


    if (1) {

        mcl_main();
    } else {
        setup_cl_context("./rawkernels/knn/Knn.cl");
        generate_field();
        define_goal(goal_x, goal_y, goal_class);
        get_euclidean();
        get_bitonic();
        get_nearestk();

    }

    return (EXIT_SUCCESS);
}

