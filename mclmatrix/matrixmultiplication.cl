// fermi



__kernel void matmulKernel(const int n, const int m, const int p, __global float* c, const __global float* a, const __global float* b) {
    const int i = get_group_id(1);
    const int bj = get_group_id(0);
    const int wtj = get_local_id(1);
    const int ttj = get_local_id(0);

    const int nrThreadsM = min(256, m);
    const int nrBlocksM = m == 1 * nrThreadsM ?
        1 :
        m % (1 * nrThreadsM) == 0 ?
            m / (1 * nrThreadsM) :
            m / (1 * nrThreadsM) + 1
    ;
    const int nrThreadsNrThreadsM = 32;
    const int nrWarpsNrThreadsM = nrThreadsM == 1 * nrThreadsNrThreadsM ?
        1 :
        nrThreadsM % (1 * nrThreadsNrThreadsM) == 0 ?
            nrThreadsM / (1 * nrThreadsNrThreadsM) :
            nrThreadsM / (1 * nrThreadsNrThreadsM) + 1
    ;
    const int tj = wtj * (1 * nrThreadsNrThreadsM) + ttj;
    if (tj < nrThreadsM) {
        const int j = bj * (1 * nrThreadsM) + tj;
        if (j < m) {
            float sum = 0.0;
            for (int k = 0; k < p; k++) {
                sum = sum + a[k + i * (1 * p)] * b[j + k * (1 * m)];
            }
            c[j + i * (1 * m)] += sum;
        }
    }
}


