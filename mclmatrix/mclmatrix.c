#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <sys/time.h>
#include<string.h>
#include <sys/types.h>
#include <CL/opencl.h>
#include"err_code.h"

cl_device_id device_id; // compute device id
cl_context context; // compute context
cl_command_queue commands; // compute command queue
cl_program program; // compute program
cl_kernel ko_matrix; // compute kernel
cl_mem d_partial_sums;

cl_mem d_a;
cl_mem d_b;
cl_mem d_c;

static FILE *fp;
static char *kernelSource;
static size_t source_size;

#define N 1010
#define M 1300
#define P 1020

#define DEVICE CL_DEVICE_TYPE_GPU

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

double mysecond() {
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp, &tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6);
}

int output_device_info(cl_device_id device_id) {

    int err; // error code returned from OpenCL calls
    cl_device_type device_type; // Parameter defining the type of the compute device
    cl_uint comp_units; // the max number of compute units on a device
    cl_char vendor_name[1024] = {0}; // string to hold vendor name for compute device
    cl_char device_name[1024] = {0}; // string to hold name of compute device
#ifdef VERBOSE
    cl_uint max_work_itm_dims;
    size_t max_wrkgrp_size;
    size_t *max_loc_size;
#endif


    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof (device_name), &device_name, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device name!\n");
        return EXIT_FAILURE;
    }
    printf(" \n Device is  %s ", device_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof (device_type), &device_type, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device type information!\n");
        return EXIT_FAILURE;
    }
    if (device_type == CL_DEVICE_TYPE_GPU)
        printf(" GPU from ");

    else if (device_type == CL_DEVICE_TYPE_CPU)
        printf("\n CPU from ");

    else
        printf("\n non  CPU or GPU processor from ");

    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof (vendor_name), &vendor_name, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device vendor name!\n");
        return EXIT_FAILURE;
    }
    printf(" %s ", vendor_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (cl_uint), &comp_units, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to access device number of compute units !\n");
        return EXIT_FAILURE;
    }
    printf(" with a max of %d compute units \n", comp_units);

#ifdef VERBOSE
    //
    // Optionally print information about work group sizes
    //
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint),
            &max_work_itm_dims, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n",
                err_code(err));
        return EXIT_FAILURE;
    }

    max_loc_size = (size_t*) malloc(max_work_itm_dims * sizeof (size_t));
    if (max_loc_size == NULL) {
        printf(" malloc failed\n");
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims * sizeof (size_t),
            max_loc_size, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n", err_code(err));
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (size_t),
            &max_wrkgrp_size, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n", err_code(err));
        return EXIT_FAILURE;
    }
    printf("work group, work item information");
    printf("\n max loc dim ");
    for (int i = 0; i < max_work_itm_dims; i++)
        printf(" %d ", (int) (*(max_loc_size + i)));
    printf("\n");
    printf(" Max work group size = %d\n", (int) max_wrkgrp_size);
#endif

    return CL_SUCCESS;

}

int setup_cl_context() {

    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }
#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    //    if (device_id == NULL)
    //        checkError(err, "Getting device");

    err = output_device_info(device_id);
    //    checkError(err, "Outputting device info");

    // Create a compute context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    //    checkError(err, "Creating context");

    // Create a command queue
    commands = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    //    checkError(err, "Creating command queue");

    // Load the kernel source code into the array source_str

    FILE *file = fopen("/home/arian/mcl/output/matrixmultiplication.cl", "r");
    if (!file) {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *kernelSource = (char *) calloc(sizeof (char), len);
    if (!kernelSource) {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    fread(kernelSource, sizeof (char), len, file);
    fclose(file);

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource, NULL, &err);
    checkError(err, "Creating program");

    // Build the program  
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof (buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }



}

void test_mcl_matrix() {

    double time;
    int err = 0;

    int n = N;
    int m = M;
    int p = P;


    float* a = malloc(n * p * sizeof (float));
    float* b = malloc(p * m * sizeof (float));
    float* c = malloc(m * n * sizeof (float));

    for (int i = 0; i < n * p; i++) {
        a[i] = (float) rand() / (float) RAND_MAX;
        //                a[i] = 2.0;
    }
    for (int i = 0; i < p * m; i++) {
        b[i] = (float) rand() / (float) RAND_MAX;
        //                b[i] = 2.0;
    }
    for (int i = 0; i < n * m; i++) {

        c[i] = 0.0;
    }

    ko_matrix = clCreateKernel(program, "matmulKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof (float)* n*p, a, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof (float)* p*m, b, &err);
    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof (float)* n*m, c, &err);

    const int nrThreadsM = MIN(256, m);
    const int nrBlocksM = m == 1 * nrThreadsM ?
            1 :
            m % (1 * nrThreadsM) == 0 ?
            m / (1 * nrThreadsM) :
            m / (1 * nrThreadsM) + 1
            ;

    const int nrThreadsNrThreadsM = 32;
    const int nrWarpsNrThreadsM = nrThreadsM == 1 * nrThreadsNrThreadsM ?
            1 :
            nrThreadsM % (1 * nrThreadsNrThreadsM) == 0 ?
            nrThreadsM / (1 * nrThreadsNrThreadsM) :
            nrThreadsM / (1 * nrThreadsNrThreadsM) + 1
            ;

    size_t global[3] = {nrThreadsNrThreadsM * nrBlocksM, nrWarpsNrThreadsM * n, 1};
    size_t local[3] = {nrThreadsNrThreadsM, nrWarpsNrThreadsM, 1};

    
    err = clSetKernelArg(ko_matrix, 0, sizeof (int), &n);
    err = clSetKernelArg(ko_matrix, 1, sizeof (int), &m);
    err = clSetKernelArg(ko_matrix, 2, sizeof (int), &p);
    err = clSetKernelArg(ko_matrix, 3, sizeof (cl_mem), &d_c);
    err = clSetKernelArg(ko_matrix, 4, sizeof (cl_mem), &d_a);
    err = clSetKernelArg(ko_matrix, 5, sizeof (cl_mem), &d_b);
    //    err = clSetKernelArg(ko_matrix, 6, sizeof (int), &nrThreadsM);
    //    err = clSetKernelArg(ko_matrix, 7, sizeof (int), &nrBlocksM);
    //    err = clSetKernelArg(ko_matrix, 8, sizeof (int), &nrThreadsNrThreadsM);
    //    err = clSetKernelArg(ko_matrix, 9, sizeof (int), &nrWarpsNrThreadsM);
    time = mysecond();
    err = clEnqueueNDRangeKernel(commands, ko_matrix, 3, NULL, global, local, 0, NULL, NULL);
    //        clFinish(commands);
    err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, sizeof (float)*n*m, c, 0, NULL, NULL);
    time = mysecond() - time;

    printf("time was: %f seconds", time);
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < p; j++) {
//                printf("%d ", (int) c[i * m + j]);
//            }
//        }

    free(a);
    free(b);
    free(c);
}

int main(int argc, char** argv) {

    setup_cl_context();
    test_mcl_matrix();
    return (EXIT_SUCCESS);
}

