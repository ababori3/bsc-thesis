// fermi

typedef struct __attribute__ ((packed)) {
    float dist;
    int class;
} Distpoint;

__kernel void bitonicKernel(const int n, __global Distpoint* data,const int stage,const int subStage, const int direction) {
    const int bi = get_group_id(0);
    const int wti = get_local_id(1);
    const int tti = get_local_id(0);

    const int nrThreadsN = min(256, n);
    const int nrBlocksN = n == 1 * nrThreadsN ?
        1 :
        n % (1 * nrThreadsN) == 0 ?
            n / (1 * nrThreadsN) :
            n / (1 * nrThreadsN) + 1
    ;
    const int nrThreadsNrThreadsN = 32;
    const int nrWarpsNrThreadsN = nrThreadsN == 1 * nrThreadsNrThreadsN ?
        1 :
        nrThreadsN % (1 * nrThreadsNrThreadsN) == 0 ?
            nrThreadsN / (1 * nrThreadsNrThreadsN) :
            nrThreadsN / (1 * nrThreadsNrThreadsN) + 1
    ;
    const int ti = wti * (1 * nrThreadsNrThreadsN) + tti;
    if (ti < nrThreadsN) {
        const int i = bi * (1 * nrThreadsN) + ti;
        if (i < n) {

    uint sortIncreasing = direction;


    uint distanceBetweenPairs = 1 << (stage - subStage);
    uint blockWidth = 2 * distanceBetweenPairs;

    uint leftId = (i % distanceBetweenPairs) +  (i / distanceBetweenPairs) * blockWidth;
    uint rightId = leftId + distanceBetweenPairs;
    
    float leftElement = data[leftId].dist;
    int leftClass = data[leftId].class;
    
    float rightElement = data[rightId].dist;
    int rightClass = data[rightId].class;

    uint sameDirectionBlockWidth = 1 << stage;
    if((i/sameDirectionBlockWidth) % 2 == 1){
        sortIncreasing = 1 - sortIncreasing;
    }
    
    float greater;
    int greaterClass;
    float lesser;
    int lesserClass;

    if(leftElement > rightElement) {
        greater = leftElement;
        greaterClass = leftClass;
        
        lesser = rightElement;
        lesserClass = rightClass;
        
    }  else {
        greater = rightElement;
        greaterClass = rightClass;
        
        lesser = leftElement;
        lesserClass = leftClass;
    }
    if(sortIncreasing) {
        data[leftId].dist = lesser;
        data[leftId].class = lesserClass;
        
        data[rightId].dist = greater;
        data[rightId].class = greaterClass;
    } else {
        data[leftId].dist = greater;
        data[leftId].class = greaterClass;
        
        data[rightId].dist = lesser;
        data[rightId].class = lesserClass;
    }  
        }
    }
}


