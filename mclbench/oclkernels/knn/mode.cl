// fermi

typedef struct __attribute__ ((packed)) {
    float dist;
    int class;
} Distpoint;



__kernel void modeKernel(const int n,const int k, __global Distpoint* input, volatile __global int* count, int compare) {
    const int bi = get_group_id(0);
    const int wti = get_local_id(1);
    const int tti = get_local_id(0);

    const int nrThreadsN = min(256, n);
    const int nrBlocksN = n == 1 * nrThreadsN ?
        1 :
        n % (1 * nrThreadsN) == 0 ?
            n / (1 * nrThreadsN) :
            n / (1 * nrThreadsN) + 1
    ;
    const int nrThreadsNrThreadsN = 32;
    const int nrWarpsNrThreadsN = nrThreadsN == 1 * nrThreadsNrThreadsN ?
        1 :
        nrThreadsN % (1 * nrThreadsNrThreadsN) == 0 ?
            nrThreadsN / (1 * nrThreadsNrThreadsN) :
            nrThreadsN / (1 * nrThreadsNrThreadsN) + 1
    ;
    const int ti = wti * (1 * nrThreadsNrThreadsN) + tti;
    if (ti < nrThreadsN) {
        const int i = bi * (1 * nrThreadsN) + ti;
        if (i < n) {
        #pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable


        
        if( i< k && input[i].class == compare){
            
            atomic_inc(count);
            }
        }
    }
}




