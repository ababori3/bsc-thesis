// fermi
typedef struct __attribute__ ((packed)) {
    float dist;
    int class;
} Distpoint;

typedef struct __attribute__ ((packed)) {
    int x;
    int y;
    int class;
} Point;



__kernel void distKernel(const int n,const Point target, __global Point* input, __global Distpoint* output) {
    const int bi = get_group_id(0);
    const int wti = get_local_id(1);
    const int tti = get_local_id(0);

    const int nrThreadsN = min(256, n);
    const int nrBlocksN = n == 1 * nrThreadsN ?
        1 :
        n % (1 * nrThreadsN) == 0 ?
            n / (1 * nrThreadsN) :
            n / (1 * nrThreadsN) + 1
    ;
    const int nrThreadsNrThreadsN = 32;
    const int nrWarpsNrThreadsN = nrThreadsN == 1 * nrThreadsNrThreadsN ?
        1 :
        nrThreadsN % (1 * nrThreadsNrThreadsN) == 0 ?
            nrThreadsN / (1 * nrThreadsNrThreadsN) :
            nrThreadsN / (1 * nrThreadsNrThreadsN) + 1
    ;
    const int ti = wti * (1 * nrThreadsNrThreadsN) + tti;
    if (ti < nrThreadsN) {
        const int i = bi * (1 * nrThreadsN) + ti;
        if (i < n) {
    		float distance = 0;
    
    		distance += pow((float)(target.x - input[i].x), 2);
    		distance += pow((float)(target.y - input[i].y), 2);
    
    		output[i].dist = sqrt(distance);
    		output[i].class = input[i].class;
        }
    }
}


