

typedef struct __attribute__ ((packed)) {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} Color;



__kernel void grayscaleKernel(const int n, __global float* output, const __global Color* input) {
    
    const int i = get_global_id(0);
        
//    const float r = (float) input[i].r;
//    const float g = (float) input[i].g;
//    const float b = (float) input[i].b;
//    output[i] = 0.299 * r + 0.587 * g + 0.114 * b;
    output[i] = 0.299 * (float) input[i].r + 0.587 * (float) input[i].g + 0.114 * (float) input[i].b;
    
    
}