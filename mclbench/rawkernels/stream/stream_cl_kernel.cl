

__kernel void test_kernel(__global float* a) {
    int id = get_global_id(0);
    a[id] = 2.0E0 * a[id];
//      a[id] = (float)id;
}

__kernel void copy_Kernel(__global float* a, __global float* c ) {
    int id = get_global_id(0);
    c[id] = a[id];
}

__kernel void scale_Kernel(__global float* a, __global float* c) {
    const float scalar = 3.0;
    int id = get_global_id(0);
    c[id] = scalar * a[id];
}

__kernel void add_Kernel(__global float* a, __global float* b, __global float* c) {
    int id = get_global_id(0);
     c[id] = b[id] + a[id];
}

__kernel void triad_Kernel( __global float* a, __global float* b, __global float* c, const double scalar) {
    int id = get_global_id(0);
    a[id] = b[id] + scalar * c[id];
}
