__constant int N_ROTATIONS = 4000000;

__kernel void crunch(__global float * out){
  float x,y,cs,sn,xx,yy; // vector_t is float or float2 or float4
  
  x = 1.0f;
  y = 0.0f;
  cs = cos(2.0f); // random angle
  sn = sin(2.0f);
  for (int i=0;i<N_ROTATIONS;i++)
  {
    xx = cs * x - sn * y;
    yy = cs * y + sn * x;
    x = xx;
    y = yy;
  }
  out[get_global_id(0)] = dot(x,y);
}
