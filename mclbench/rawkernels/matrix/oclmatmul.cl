

__kernel void matmulKernel(const int n,const int m,const int p,__global float* C, __global float* A,__global float* B){                                                                       
    const int globalRow = get_global_id(0); // Row ID of C (0..M)
    const int globalCol = get_global_id(1); // Col ID of C (0..N)

    // Compute a single element (loop over K)
    float acc = 0.0f;
    for (int k=0; k<p; k++) {
        acc += A[k*n + globalRow] * B[globalCol* p + k];
    }

    // Store the result
    C[globalCol*n + globalRow] = acc;
}                                                                       
