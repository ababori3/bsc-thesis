
typedef struct Point {
    int x;
    int y;
    int class;

} Point;

typedef struct Dist_point {
    float dist;
    int class;

} Dist_point;

kernel void distKernel(const int n, const Point target, const __global Point* input, __global Dist_point* output) {
    
    const int i = get_global_id(0);
    float distance = 0;
    
    distance += pow((float)(target.x - input[i].x), 2);
    distance += pow((float)(target.y - input[i].y), 2);
    
    output[i].dist = sqrt(distance);
    output[i].class = input[i].class;
}


//kernel void bitonicKernel(const int n){
//    
//    const int i = get_global_id(0);
//    
//    for(int i = 1;i<n;i*=2){
//        
//        for(int j = i;j<=1;j/=2){
//            
//            }
//        }
//    
//    }
    
__kernel void bitonicKernel(const int n, __global Dist_point* data,const uint stage,const uint subStage, const uint direction) {
    uint sortIncreasing = direction;
    uint threadId = get_global_id(0);

    uint distanceBetweenPairs = 1 << (stage - subStage);
    uint blockWidth = 2 * distanceBetweenPairs;

    uint leftId = (threadId % distanceBetweenPairs) +  (threadId / distanceBetweenPairs) * blockWidth;
    uint rightId = leftId + distanceBetweenPairs;
    
    float leftElement = data[leftId].dist;
    int leftClass = data[leftId].class;
    
    float rightElement = data[rightId].dist;
    int rightClass = data[rightId].class;

    uint sameDirectionBlockWidth = 1 << stage;
    if((threadId/sameDirectionBlockWidth) % 2 == 1){
        sortIncreasing = 1 - sortIncreasing;
    }
    
    float greater;
    int greaterClass;
    float lesser;
    int lesserClass;

    if(leftElement > rightElement) {
        greater = leftElement;
        greaterClass = leftClass;
        
        lesser = rightElement;
        lesserClass = rightClass;
        
    }  else {
        greater = rightElement;
        greaterClass = rightClass;
        
        lesser = leftElement;
        lesserClass = leftClass;
    }
    if(sortIncreasing) {
        data[leftId].dist = lesser;
        data[leftId].class = lesserClass;
        
        data[rightId].dist = greater;
        data[rightId].class = greaterClass;
    } else {
        data[leftId].dist = greater;
        data[leftId].class = greaterClass;
        
        data[rightId].dist = lesser;
        data[rightId].class = lesserClass;
    }   

}

__kernel void modeKernel(const int n,const int k, __global Dist_point* input, volatile __global int* count, int compare){
        #pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

        int i = get_global_id(0);
        
        if( i< k && input[i].class == compare){
            
            atomic_inc(count);
            }
        
    }
