#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <sys/time.h>
#include<string.h>
#include <sys/types.h>
#include <CL/opencl.h>
#include <stdio_ext.h>
#include <math.h>
#include "err_code.h"

//stream

#ifndef STREAM_ARRAY_SIZE
#define STREAM_ARRAY_SIZE 10000000
#endif

#ifdef NTIMES
#if NTIMES<=1
#define NTIMES 10
#endif
#endif
#ifndef NTIMES
#define NTIMES 10
#endif 


#ifndef OFFSET
#define OFFSET 0
#endif

#define HLINE "-------------------------------------------------------------\n"

#ifndef STREAM_TYPE
#define STREAM_TYPE double
#endif

#define TM 20
#define MAX_SOURCE_SIZE 25000
#define VERBOSE

static STREAM_TYPE a[STREAM_ARRAY_SIZE + OFFSET],
b[STREAM_ARRAY_SIZE + OFFSET],
c[STREAM_ARRAY_SIZE + OFFSET];

static double avgtime[4] = {0}, maxtime[4] = {0},
mintime[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

static char *label[4] = {"Copy:      ", "Scale:     ",
    "Add:       ", "Triad:     "};

static double bytes[4] = {
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE
};

static int quantum, checktick(), k;
static int BytesPerWord;
static STREAM_TYPE scalar;
static double t, times[4][NTIMES];
static size_t workr = (size_t) (STREAM_ARRAY_SIZE);
static size_t local_work = 512;

//gray
#define PIXELS 20700000
//#define PIXELS 2147483646


//matrix

#define MATMUL_ITER 1

#define N 2500
#define M 2500
#define P 2500

//pi
//long long INSTEPS = (512 * 512 * 512 * 2);
const long long INSTEPS = (512 * 512 * 512 * 4);
#define ITERS (512)

//#define ASIZE 5120
//
//int a_pi[ASIZE];
//int c_pi[ASIZE];
//size_t workrpi = ASIZE;
//size_t local_workpi = 512;

//knn

#ifndef KNNP
#define KNNP 20
#endif

#ifndef KNNK
#define KNNK 20
#endif

typedef struct Point {
    int x;
    int y;
    int class;

} Point;

typedef struct Dist_point {
    float dist;
    int class;

} Dist_point;


int goal_x = 200;
int goal_y = 75;
int goal_class = 0;

Point* field;
Point target;

Dist_point* distances;

size_t knnp = pow(2, KNNP);

//global

cl_uint comp_units; // the max number of compute units on a device
cl_device_type device_type; // Parameter defining the type of the compute device
cl_char vendor_name[1024] = {0}; // string to hold vendor name for compute device
cl_char device_name[1024] = {0}; // string to hold name of compute device
#ifdef VERBOSE
cl_uint max_work_itm_dims;
size_t max_wrkgrp_size;
size_t *max_loc_size;
#endif


cl_device_id device_id; // compute device id
cl_context context; // compute context
cl_command_queue commands; // compute command queue
cl_program program; // compute program

cl_kernel ko_stream;
cl_kernel ko_crunch;
cl_kernel ko_gray;
cl_kernel ko_matrix;
cl_kernel ko_pi;
cl_kernel ko_nearest;
cl_kernel ko_bitonic;
cl_kernel ko_mode;

cl_mem d_partial_sums;
cl_mem d_a;
cl_mem d_b;
cl_mem d_c;

static FILE *fp;
static char *kernelSource;
static size_t source_size;

static FILE *bm_log;

#define DEVICE CL_DEVICE_TYPE_GPU

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

void init_log() {
    //    fclose(fopen("bm_log.txt", "w"));
    //    bm_log = fopen("bm_log.txt", "a+");
    bm_log = fopen("bm_log.txt", "w");

}

int output_device_info(cl_device_id device_id) {

    int err; // error code returned from OpenCL calls

    err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof (device_name), &device_name, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device name!\n");
        return EXIT_FAILURE;
    }
    fprintf(bm_log, " \n Device is  %s ", device_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof (device_type), &device_type, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device type information!\n");
        return EXIT_FAILURE;
    }
    if (device_type == CL_DEVICE_TYPE_GPU)
        fprintf(bm_log, " GPU from ");

    else if (device_type == CL_DEVICE_TYPE_CPU)
        fprintf(bm_log, "\n CPU from ");

    else
        fprintf(bm_log, "\n non  CPU or GPU processor from ");

    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof (vendor_name), &vendor_name, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device vendor name!\n");
        return EXIT_FAILURE;
    }
    fprintf(bm_log, " %s ", vendor_name);

    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (cl_uint), &comp_units, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to access device number of compute units !\n");
        return EXIT_FAILURE;
    }
    fprintf(bm_log, " with a max of %d compute units \n", comp_units);

#ifdef VERBOSE
    //
    // Optionally print information about work group sizes
    //
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint),
            &max_work_itm_dims, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)!\n%s\n",
                err_code(err));
        return EXIT_FAILURE;
    }

    max_loc_size = (size_t*) malloc(max_work_itm_dims * sizeof (size_t));
    if (max_loc_size == NULL) {
        fprintf(bm_log, " malloc failed\n");
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims * sizeof (size_t),
            max_loc_size, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to get device Info (CL_DEVICE_MAX_WORK_ITEM_SIZES)!\n%s\n", err_code(err));
        return EXIT_FAILURE;
    }
    err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (size_t),
            &max_wrkgrp_size, NULL);
    if (err != CL_SUCCESS) {
        fprintf(bm_log, "Error: Failed to get device Info (CL_DEVICE_MAX_WORK_GROUP_SIZE)!\n%s\n", err_code(err));
        return EXIT_FAILURE;
    }
    fprintf(bm_log, "work group, work item information");
    fprintf(bm_log, "\n max loc dim ");
    for (int i = 0; i < max_work_itm_dims; i++)
        fprintf(bm_log, " %d ", (int) (*(max_loc_size + i)));
    fprintf(bm_log, "\n");
    fprintf(bm_log, " Max work group size = %d\n", (int) max_wrkgrp_size);
#endif

    return CL_SUCCESS;

}

void intro() {

    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        fprintf(bm_log, "Found 0 platforms!\n");
        exit(1);
    }
#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Getting device");

    err = output_device_info(device_id);

    /* --- SETUP --- determine precision and check timing --- */
    int BytesPerWord;

    BytesPerWord = sizeof (STREAM_TYPE);
    fprintf(bm_log, "This system uses %d bytes per array element.\n",
            BytesPerWord);

    fprintf(bm_log, HLINE);

    fprintf(bm_log, "Array size = %llu (elements), Offset = %d (elements)\n", (unsigned long long) STREAM_ARRAY_SIZE, OFFSET);
    fprintf(bm_log, "Memory per array = %.1f MiB (= %.1f GiB).\n",
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0),
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0 / 1024.0));
    fprintf(bm_log, "Total memory required = %.1f MiB (= %.1f GiB).\n",
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.),
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024. / 1024.));
    fprintf(bm_log, "Each kernel will be executed %d times.\n", NTIMES);
    fprintf(bm_log, " The *best* time for each kernel (excluding the first iteration)\n");
    fprintf(bm_log, " will be used to compute the reported bandwidth.\n");
}

double mysecond() {
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp, &tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6);
}

int setup_cl_context(char *input) {

    // Set up platform and GPU device
    cl_int err;
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    //    checkError(err, "Finding platforms");
    if (numPlatforms == 0) {
        fprintf(bm_log, "Found 0 platforms!\n");
        return EXIT_FAILURE;
    }
#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    //    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++) {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS) {
            break;
        }
    }

    //    if (device_id == NULL)
    //        checkError(err, "Getting device");

    //    err = output_device_info(device_id);
    //    checkError(err, "Outputting device info");

    // Create a compute context 
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    //    checkError(err, "Creating context");

    // Create a command queue
    commands = clCreateCommandQueueWithProperties(context, device_id, 0, &err);
    //    checkError(err, "Creating command queue");

    // Load the kernel source code into the array source_str

    //    FILE *file = fopen("/home/arian/mcl/output/matrixmultiplication.cl", "r");
    FILE *file = fopen(input, "r");
    if (!file) {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *kernelSource = (char *) calloc(sizeof (char), len);
    if (!kernelSource) {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    fread(kernelSource, sizeof (char), len, file);
    fclose(file);

    // Create the compute program from the source buffer
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource, NULL, &err);
    checkError(err, "Creating program");

    //free kernelSource
    free(kernelSource);

    // Build the program  
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        fprintf(bm_log, "Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof (buffer), buffer, &len);
        fprintf(bm_log, "%s\n", buffer);
        return EXIT_FAILURE;
    }

}

//stream

int checktick() {

    int i, minDelta, Delta;
    double t1, t2, timesfound[M];

    /*  Collect a sequence of TM unique time values from the system. */

    for (i = 0; i < TM; i++) {
        t1 = mysecond();
        while (((t2 = mysecond()) - t1) < 1.0E-6)
            ;
        timesfound[i] = t1 = t2;
    }

    /*
     * Determine the minimum difference between these M values.
     * This result will be our estimate (in microseconds) for the
     * clock granularity.
     */

    minDelta = 1000000;
    for (i = 1; i < M; i++) {
        Delta = (int) (1.0E6 * (timesfound[i] - timesfound[i - 1]));
        minDelta = MIN(minDelta, MAX(Delta, 0));
    }

    return (minDelta);
}

void init_stream_arrays() {
    for (int j = 0; j < STREAM_ARRAY_SIZE; j++) {
        a[j] = 1.0;
        b[j] = 2.0;
        c[j] = 0.0;
    }
}

int check_granularity(int quantum) {

    fprintf(bm_log, HLINE);

    if ((quantum = checktick()) >= 1) {
        fprintf(bm_log, "Your clock granularity/precision appears to be " "%d microseconds.\n", quantum);
    } else {
        fprintf(bm_log, "Your clock granularity appears to be "
                "less than one microsecond.\n");
        quantum = 1;
    }
    return quantum;
}

void get_test_times(double t, int quantum) {

    int err = 0;
    t = mysecond();

    ko_stream = clCreateKernel(program, "test_Kernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    err = clSetKernelArg(ko_stream, 0, sizeof (cl_mem), &d_a);
    err = clEnqueueNDRangeKernel(commands, ko_stream, 1, NULL, &workr, &local_work, 0, NULL, NULL);
    err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, a, 0, NULL, NULL);
    checkError(err, "Enqueueing kernel 1st time");
    t = 1.0E6 * (mysecond() - t);

    fprintf(bm_log, "Each test below will take on the order"
            " of %d microseconds.\n", (int) t);
    fprintf(bm_log, "   (= %d clock ticks)\n", (int) (t / quantum));
    fprintf(bm_log, "Increase the size of the arrays if this shows that\n");
    fprintf(bm_log, "you are not getting at least 20 clock ticks per test.\n");

    fprintf(bm_log, HLINE);

    fprintf(bm_log, "WARNING -- The above is only a rough guideline.\n");
    fprintf(bm_log, "For best results, please be sure you know the\n");
    fprintf(bm_log, "precision of your system timer.\n");
    fprintf(bm_log, HLINE);
}

void test_copy_mcl() {

    init_stream_arrays();
    int err = 0;
    int n = STREAM_ARRAY_SIZE;

    ko_stream = clCreateKernel(program, "copy_Kernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, c, &err);

    for (k = 0; k < NTIMES; k++) {

        times[0][k] = mysecond();
        err = clSetKernelArg(ko_stream, 0, sizeof (int), &n);
        err = clSetKernelArg(ko_stream, 1, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_stream, 2, sizeof (cl_mem), &d_c);
        err = clEnqueueNDRangeKernel(commands, ko_stream, 1, NULL, &workr, &max_wrkgrp_size, 0, NULL, NULL);
        //        clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[0][k] = mysecond() - times[0][k];
    }


}

void test_scale_mcl() {

    init_stream_arrays();
    int err = 0;
    int n = STREAM_ARRAY_SIZE;
    float scalar = 3.0;
    ko_stream = clCreateKernel(program, "scale_Kernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, c, &err);

    for (k = 0; k < NTIMES; k++) {
        times[1][k] = mysecond();
        err = clSetKernelArg(ko_stream, 0, sizeof (int), &n);
        err = clSetKernelArg(ko_stream, 1, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_stream, 2, sizeof (cl_mem), &d_c);
        err = clSetKernelArg(ko_stream, 3, sizeof (float), &scalar);
        //        err = clSetKernelArg(ko_stream, 2, sizeof (double), &scalar);
        err = clEnqueueNDRangeKernel(commands, ko_stream, 1, NULL, &workr, &max_wrkgrp_size, 0, NULL, NULL);
        //                clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[1][k] = mysecond() - times[1][k];
    }
}

void test_add_mcl() {

    init_stream_arrays();
    int err = 0;
    int n = STREAM_ARRAY_SIZE;
    ko_stream = clCreateKernel(program, "add_Kernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, b, &err);
    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, c, &err);

    for (k = 0; k < NTIMES; k++) {
        times[2][k] = mysecond();
        err = clSetKernelArg(ko_stream, 0, sizeof (int), &n);
        err = clSetKernelArg(ko_stream, 1, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_stream, 2, sizeof (cl_mem), &d_b);
        err = clSetKernelArg(ko_stream, 3, sizeof (cl_mem), &d_c);
        //        err = clSetKernelArg(ko_stream, 2, sizeof (double), &scalar);
        err = clEnqueueNDRangeKernel(commands, ko_stream, 1, NULL, &workr, &max_wrkgrp_size, 0, NULL, NULL);
        //                clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[2][k] = mysecond() - times[2][k];
    }
}

void test_triad_mcl() {

    init_stream_arrays();
    int err = 0;
    int n = STREAM_ARRAY_SIZE;
    ko_stream = clCreateKernel(program, "triad_Kernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, a, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, b, &err);
    d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, STREAM_ARRAY_SIZE * 4, c, &err);

    for (k = 0; k < NTIMES; k++) {
        times[3][k] = mysecond();
        err = clSetKernelArg(ko_stream, 0, sizeof (int), &n);
        err = clSetKernelArg(ko_stream, 1, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_stream, 2, sizeof (cl_mem), &d_b);
        err = clSetKernelArg(ko_stream, 3, sizeof (cl_mem), &d_c);
        err = clSetKernelArg(ko_stream, 4, sizeof (float), &scalar);
        //        err = clSetKernelArg(ko_stream, 2, sizeof (double), &scalar);
        err = clEnqueueNDRangeKernel(commands, ko_stream, 1, NULL, &workr, &max_wrkgrp_size, 0, NULL, NULL);
        //                clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, STREAM_ARRAY_SIZE * 4, c, 0, NULL, NULL);
        times[3][k] = mysecond() - times[3][k];
    }
}

void print_results() {
    for (k = 1; k < NTIMES; k++) /* note -- skip first iteration */ {
        for (int j = 0; j < 4; j++) {
            avgtime[j] = avgtime[j] + times[j][k];
            mintime[j] = MIN(mintime[j], times[j][k]);
            maxtime[j] = MAX(maxtime[j], times[j][k]);
        }
    }

    fprintf(bm_log, "Function    Best Rate MB/s  Avg time     Min time     Max time\n");
    for (int j = 0; j < 4; j++) {
        avgtime[j] = avgtime[j] / (double) (NTIMES - 1);

        fprintf(bm_log, "%s%12.1f  %11.6f  %11.6f  %11.6f\n", label[j],
                1.0E-06 * bytes[j] / mintime[j],
                avgtime[j],
                mintime[j],
                maxtime[j]);
    }
    fprintf(bm_log, HLINE);
}

void test_stream() {

    setup_cl_context("./oclkernels/stream/streamTest.cl");
    quantum = check_granularity(quantum);
    get_test_times(t, quantum);
    setup_cl_context("./oclkernels/stream/streamCopy.cl");
    test_copy_mcl();

    setup_cl_context("./oclkernels/stream/streamScale.cl");
    test_scale_mcl();

    setup_cl_context("./oclkernels/stream/streamAdd.cl");
    test_add_mcl();

    setup_cl_context("./oclkernels/stream/streamTriad.cl");
    test_triad_mcl();

    print_results();
}

void test_stream_ocl() {

    setup_cl_context("./rawkernels/stream/stream_cl_kernel.cl");
    quantum = check_granularity(quantum);
    get_test_times(t, quantum);
    test_copy_mcl();

    test_scale_mcl();

    test_add_mcl();

    test_triad_mcl();

    print_results();
}

//crunch

void test_crunch() {

    int rotations = 4000000;
    int err;
    double rtime;

    size_t glob = max_wrkgrp_size * comp_units;
    size_t loc = max_wrkgrp_size;

    float *d = malloc(glob * sizeof (float));

    ko_crunch = clCreateKernel(program, "crunch", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, glob * 4, d, &err);

    err = clSetKernelArg(ko_crunch, 0, sizeof (cl_mem), &d_a);

    rtime = mysecond();
    err = clEnqueueNDRangeKernel(commands, ko_crunch, 1, NULL, &glob, &loc, 0, NULL, NULL);
    checkError(err, "enqueing kernel");
    //                clFinish(commands);
    err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, glob * 4, d, 0, NULL, NULL);
    rtime = mysecond() - rtime;

    fprintf(bm_log, "time is %f sec\n", rtime);
    fprintf(bm_log, "pmax = %Lf\n", (long double) 6 * 4000000 * glob * 4 / rtime / 1000000000);
    fprintf(bm_log, HLINE);

}

//grayscale

void test_grayscale_mcl() {

    typedef struct Color {
        unsigned char r;
        unsigned char g;
        unsigned char b;
    } Color;

    int err;
    double rtime;
    size_t npix = (PIXELS / max_wrkgrp_size) * max_wrkgrp_size;
    //    size_t npix = PIXELS;
    size_t glob = npix * sizeof (Color);
    size_t gloc = MIN(max_wrkgrp_size, npix);
    float *output = malloc(npix * sizeof (float));

    //    printf("hey its %d", npix);


    //    Color rainbow[npix];
    Color* rainbow = malloc(npix * sizeof (Color));

    ko_gray = clCreateKernel(program, "grayscaleKernel", &err);
    checkError(err, "Creating kernel");

    for (int i = 0; i < npix; i++) {


        int r = rand() % 255;
        int g = rand() % 255;
        int b = rand() % 255;

        //                int r = 4;
        //                int g = 10;
        //                int b = 212;

        rainbow[i].r = (char) (r);
        rainbow[i].g = (char) (g);
        rainbow[i].b = (char) (b);
    }

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, npix * sizeof (Color), rainbow, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, npix * sizeof (float), output, &err);

    err = clSetKernelArg(ko_gray, 0, sizeof (int), &npix);
    err = clSetKernelArg(ko_gray, 1, sizeof (cl_mem), &d_b);
    err = clSetKernelArg(ko_gray, 2, sizeof (cl_mem), &d_a);

    rtime = mysecond();
    err = clEnqueueNDRangeKernel(commands, ko_gray, 1, NULL, &glob, &gloc, 0, NULL, NULL);
    checkError(err, "enqueing kernel");
    //                clFinish(commands);
    err = clEnqueueReadBuffer(commands, d_b, CL_TRUE, 0, npix * 4, output, 0, NULL, NULL);
    rtime = mysecond() - rtime;
    long double resulti = (long double) npix * 5 / rtime / 1000000000;
    fprintf(bm_log, "grayscale time is %f sec\n", rtime);
    //    fprintf(bm_log, "grayscale Gflops is %Lf \n", (long double) npix * 5 / rtime / 100000000);
    fprintf(bm_log, "grayscale Gflops is %Lf \n", resulti);
    fprintf(bm_log, HLINE);

    //    for(int i=0;i<npix;i++){
    //        printf("%f ", output[i]);
    //    }


}

//pi

int test_pi_mcl() {

    int err;
    float *h_psum; // vector to hold partial sum
    int in_nsteps = INSTEPS; // default number of steps (updated later to device preferable)
    int niters = ITERS; // number of iterations
    int nsteps;
    float step_size;
    size_t nwork_groups;
    size_t max_size, work_group_size = 8;
    float pi_res;

    ko_pi = clCreateKernel(program, "testKernel", &err);
    checkError(err, "Creating kernel");

    // Find kernel work-group size
    err = clGetKernelWorkGroupInfo(ko_pi, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof (size_t), &work_group_size, NULL);
    checkError(err, "Getting kernel work group info");
    // Now that we know the size of the work-groups, we can set the number of
    // work-groups, the actual number of steps, and the step size
    nwork_groups = in_nsteps / (work_group_size * niters);

    if (nwork_groups < 1) {
        err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (size_t), &nwork_groups, NULL);
        checkError(err, "Getting device compute unit info");
        work_group_size = in_nsteps / (nwork_groups * niters);
    }

    nsteps = work_group_size * niters * nwork_groups;
    step_size = 1.0f / (float) nsteps;
    h_psum = calloc(sizeof (float), nwork_groups);
    if (!h_psum) {
        fprintf(bm_log, "Error: could not allocate host memory for h_psum\n");
        return EXIT_FAILURE;
    }

    fprintf(bm_log, " %ld work-groups of size %ld. %d Integration steps\n",
            nwork_groups,
            work_group_size,
            nsteps);

    d_partial_sums = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof (float) * nwork_groups, NULL, &err);
    checkError(err, "Creating buffer d_partial_sums");

    // Set kernel arguments
    err |= clSetKernelArg(ko_pi, 0, sizeof (int), &nwork_groups);
    err |= clSetKernelArg(ko_pi, 1, sizeof (int), &work_group_size);
    err = clSetKernelArg(ko_pi, 2, sizeof (int), &niters);
    err |= clSetKernelArg(ko_pi, 3, sizeof (float), &step_size);
    err |= clSetKernelArg(ko_pi, 4, sizeof (cl_mem), &d_partial_sums);
    err |= clSetKernelArg(ko_pi, 5, sizeof (float) * work_group_size, NULL);

    checkError(err, "Settin kernel args");

    // Execute the kernel over the entire range of our 1D input data set
    // using the maximum number of work items for this device
    size_t global = nsteps / niters;
    size_t local = work_group_size;
    double rtime = mysecond();
    err = clEnqueueNDRangeKernel(
            commands,
            ko_pi,
            1, NULL,
            &global,
            &local,
            0, NULL, NULL);
    checkError(err, "Enqueueing kernel");

    err = clEnqueueReadBuffer(
            commands,
            d_partial_sums,
            CL_TRUE,
            0,
            sizeof (float) * nwork_groups,
            h_psum,
            0, NULL, NULL);
    checkError(err, "Reading back d_partial_sums");

    // complete the sum and compute the final integral value on the host
    pi_res = 0.0f;
    for (unsigned int i = 0; i < nwork_groups; i++) {
        pi_res += h_psum[i];
    }
    pi_res *= step_size;

    rtime = mysecond() - rtime;

    free(h_psum);
    fprintf(bm_log, "\nThe calculation ran in %11.6f seconds\n", rtime);
    fprintf(bm_log, " pi = %f for %d steps\n", pi_res, nsteps);
    fprintf(bm_log, HLINE);
}

//matmul

void test_mcl_matrix() {


    double time[MATMUL_ITER];
    int err = 0;

    int n = N;
    int m = M;
    int p = P;

    float* a = malloc(n * p * sizeof (float));
    float* b = malloc(p * m * sizeof (float));
    float* c = malloc(m * n * sizeof (float));

    for (int tm = 0; tm < MATMUL_ITER; tm++) {



        for (int i = 0; i < n * p; i++) {
            a[i] = (float) rand() / (float) RAND_MAX;
            //                a[i] = 2.0;
        }
        for (int i = 0; i < p * m; i++) {
            b[i] = (float) rand() / (float) RAND_MAX;
            //                b[i] = 2.0;
        }
        for (int i = 0; i < n * m; i++) {

            c[i] = 0.0;
        }

        ko_matrix = clCreateKernel(program, "matmulKernel", &err);
        checkError(err, "Creating kernel");

        d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof (float)* n*p, a, &err);
        d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof (float)* p*m, b, &err);
        d_c = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof (float)* n*m, c, &err);

        const int nrThreadsM = MIN(256, m);
        const int nrBlocksM = m == 1 * nrThreadsM ?
                1 :
                m % (1 * nrThreadsM) == 0 ?
                m / (1 * nrThreadsM) :
                m / (1 * nrThreadsM) + 1
                ;

        const int nrThreadsNrThreadsM = 32;
        const int nrWarpsNrThreadsM = nrThreadsM == 1 * nrThreadsNrThreadsM ?
                1 :
                nrThreadsM % (1 * nrThreadsNrThreadsM) == 0 ?
                nrThreadsM / (1 * nrThreadsNrThreadsM) :
                nrThreadsM / (1 * nrThreadsNrThreadsM) + 1
                ;

        size_t global[2] = {nrThreadsNrThreadsM * nrBlocksM, nrWarpsNrThreadsM * n};
        size_t local[2] = {nrThreadsNrThreadsM, nrWarpsNrThreadsM};


        err = clSetKernelArg(ko_matrix, 0, sizeof (int), &n);
        err = clSetKernelArg(ko_matrix, 1, sizeof (int), &m);
        err = clSetKernelArg(ko_matrix, 2, sizeof (int), &p);
        err = clSetKernelArg(ko_matrix, 3, sizeof (cl_mem), &d_c);
        err = clSetKernelArg(ko_matrix, 4, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_matrix, 5, sizeof (cl_mem), &d_b);
        //    err = clSetKernelArg(ko_matrix, 6, sizeof (int), &nrThreadsM);
        //    err = clSetKernelArg(ko_matrix, 7, sizeof (int), &nrBlocksM);
        //    err = clSetKernelArg(ko_matrix, 8, sizeof (int), &nrThreadsNrThreadsM);
        //    err = clSetKernelArg(ko_matrix, 9, sizeof (int), &nrWarpsNrThreadsM);
        time[tm] = mysecond();
        err = clEnqueueNDRangeKernel(commands, ko_matrix, 2, NULL, global, local, 0, NULL, NULL);
        //        clFinish(commands);
        err = clEnqueueReadBuffer(commands, d_c, CL_TRUE, 0, sizeof (float)*n*m, c, 0, NULL, NULL);
        time[tm] = mysecond() - time[tm];

        fprintf(bm_log, "Matrix multiplication of dimensions %d x %d x %d\n", N, M, P);
        fprintf(bm_log, "Matrix multiplication runtime: %f seconds\n", time[tm]);
        fprintf(bm_log, HLINE);

        //                for (int i = 0; i < n; i++) {
        //                    for (int j = 0; j < p; j++) {
        //                        printf( "%d ", (int) c[i * m + j]);
        //                    }
        //                }
    }

    long double avg = 0;
    for (int i = 0; i < MATMUL_ITER; i++) {
        avg += time[i] / MATMUL_ITER;
    }
    //    long double gflopRate = (N) * (M ) * (P) * 2 / avg / 1000000000;
    fprintf(bm_log, "Average runtime: %Lf  GFLOPS: %Lf\n", avg, (long double) n * m * p * 2 / avg / 1000000000);

    free(a);
    free(b);
    free(c);
}

//KNN

void generate_field() {

    size_t knnp = pow(2, KNNP);
    field = malloc(knnp * sizeof (Point));

    for (int i = 0; i < knnp; i++) {

        field[i].x = rand() % 255;
        field[i].y = rand() % 255;
        int clas = rand() % 9;
        field[i].class = (clas > 0) ? clas : 1;

    }

    //    for (int j = 0; j < knnp; j++) {
    //        printf("%d %d %d\n", field[j].x, field[j].y, field[j].class);
    //    }

}

void define_goal(int x_input, int y_input, int class_input) {

    //    goal = malloc(sizeof(Point));
    if (x_input > 200 || y_input > 200 || class_input > 8) {
        printf("incorrect size");
        exit(1);
    }

    target.x = x_input;
    target.y = y_input;
    target.class = class_input;

}

void get_euclidean() {

    size_t knnp = pow(2, KNNP);
    distances = malloc(knnp * sizeof (Dist_point));

    int err;
    double rtime;
    size_t gloc = MIN(max_wrkgrp_size, knnp);
    size_t workknn = knnp * sizeof (Point);

    ko_nearest = clCreateKernel(program, "distKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Point), field, &err);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), distances, &err);

    err = clSetKernelArg(ko_nearest, 0, sizeof (int), &knnp);
    err = clSetKernelArg(ko_nearest, 1, sizeof (Point), &target);
    err = clSetKernelArg(ko_nearest, 2, sizeof (cl_mem), &d_a);
    err = clSetKernelArg(ko_nearest, 3, sizeof (cl_mem), &d_b);

    err = clEnqueueNDRangeKernel(commands, ko_nearest, 1, NULL, &workknn, &gloc, 0, NULL, NULL);
    err = clEnqueueReadBuffer(commands, d_b, CL_TRUE, 0, knnp * sizeof (Dist_point), distances, 0, NULL, NULL);

    clReleaseKernel(ko_nearest);
    //    printf("DISTANCES------------------\n\n");
    //
    //    for (int j = 0; j < knnp; j++) {
    //        printf("%f %d \n", distances[j].dist, distances[j].class);
    //    }


}

void get_bitonic() {

    size_t knnp = pow(2, KNNP);
    size_t stages = log(knnp) / log(2);
    size_t direction = 1;
    //        Dist_point* input = malloc(knnp * sizeof (Dist_point));
    //        for (int i = 0; i < 5; i++) {
    //            input[i].dist = 2;
    //            input[i].class = rand() % 8;
    //        }
    //    
    //        for (int i = 5; i < knnp; i++) {
    //            input[i].dist = rand() % 225;
    //            input[i].class = rand() % 8;
    //        }
    //
    //
    //
    //        for (int i = 0; i < knnp; i++) {
    //            printf("%f ", input[i].dist);
    //        }

    int err;
    double rtime;

    size_t gloc = MIN(max_wrkgrp_size, knnp);

    ko_bitonic = clCreateKernel(program, "bitonicKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), distances, &err);
    //     d_a = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), input, &err);

    err = clSetKernelArg(ko_bitonic, 0, sizeof (int), &knnp);
    err = clSetKernelArg(ko_bitonic, 1, sizeof (cl_mem), &d_a);
    err = clSetKernelArg(ko_bitonic, 4, sizeof (uint), &direction);


    for (cl_uint stage = 0; stage < stages; ++stage) {
        err = clSetKernelArg(ko_bitonic, 2, sizeof (cl_uint), &stage);

        for (cl_uint subStage = 0; subStage < stage + 1; subStage++) {
            err = clSetKernelArg(ko_bitonic, 3, sizeof (cl_uint), &subStage);
            err = clEnqueueNDRangeKernel(commands, ko_bitonic, 1, NULL, &knnp, &gloc, 0, NULL, NULL);
            err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, knnp * sizeof (Dist_point), distances, 0, NULL, NULL);
            //            err = clEnqueueReadBuffer(commands, d_a, CL_TRUE, 0, knnp * sizeof (Dist_point), input, 0, NULL, NULL);



        }

    }

    clReleaseKernel(ko_bitonic);
    //    printf("sorted-----------------\n ");
    //    for (int i = 0; i < knnp; i++) {
    //        printf("%f %d\n", distances[i].dist, distances[i].class);
    //    }

    //        for (int i = 0; i < knnp; i++) {
    //        printf("%f %d\n", input[i].dist, input[i].class);
    //    }



}

void get_nearestk() {

    int err;
    double rtime;
    size_t gloc = MIN(max_wrkgrp_size, knnp);
    size_t workknn = knnp * sizeof (Dist_point);
    size_t k = KNNK;
    int count;
    int maxCount = 0;
    int maxValue = 0;

    ko_mode = clCreateKernel(program, "modeKernel", &err);
    checkError(err, "Creating kernel");

    d_a = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, knnp * sizeof (Dist_point), distances, &err);


    for (int i = 1; i <= 8; i++) {

        count = 0;
        d_b = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof (int), &count, &err);

        err = clSetKernelArg(ko_mode, 0, sizeof (size_t), &knnp);
        err = clSetKernelArg(ko_mode, 1, sizeof (size_t), &k);
        err = clSetKernelArg(ko_mode, 2, sizeof (cl_mem), &d_a);
        err = clSetKernelArg(ko_mode, 3, sizeof (cl_mem), &d_b);
        err = clSetKernelArg(ko_mode, 4, sizeof (int), &i);
        err = clEnqueueNDRangeKernel(commands, ko_mode, 1, NULL, &workknn, &gloc, 0, NULL, NULL);
        err = clEnqueueReadBuffer(commands, d_b, CL_TRUE, 0, sizeof (int), &count, 0, NULL, NULL);

        if (count > maxCount) {
            maxCount = count;
            maxValue = i;
        }

    }

    clReleaseKernel(ko_mode);
    printf("class = %d with amount %d\n", maxValue, maxCount);


}

void mcl_knn() {



    generate_field();
    define_goal(goal_x, goal_y, goal_class);

    t = mysecond();
    setup_cl_context("./oclkernels/knn/dist.cl");
    get_euclidean();

    setup_cl_context("./oclkernels/knn/bitonic.cl");
    get_bitonic();
    setup_cl_context("./oclkernels/knn/mode.cl");
    get_nearestk();

    t = mysecond() - t;


    fprintf(bm_log, "knn MCL time is: %f\n", t);
    fprintf(bm_log, HLINE);
}

void ocl_knn() {


    generate_field();
    define_goal(goal_x, goal_y, goal_class);

    t = mysecond();
    setup_cl_context("./rawkernels/knn/Knn.cl");
    get_euclidean();
    get_bitonic();
    get_nearestk();

    t = t - mysecond();

    fprintf(bm_log, "knn time is: %f", t);
}

//clean

void clean_cl_variables() {

    clReleaseMemObject(d_partial_sums);
    clReleaseMemObject(d_a);
    clReleaseMemObject(d_b);
    clReleaseMemObject(d_c);
    clReleaseProgram(program);
    clReleaseKernel(ko_stream);
    clReleaseKernel(ko_pi);
    clReleaseKernel(ko_matrix);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);

}

//main

void ocl_main() {

    printf("mcl benchmark initialized\n");
    printf("0%% complete [                  ]\n");

    test_stream();
    system("clear");
    printf("mcl_stream finished\n");
    printf("34%% complete [======          ]\n");

    setup_cl_context("./rawkernels/crunch/crunch.cl");
    test_crunch();
    system("clear");
    printf("mcl_stream finished\n");
    printf("50%% complete [========        ]\n");

    setup_cl_context("./rawkernels/grayscale/oclgrayscale.cl");
    test_grayscale_mcl();
    system("clear");
    printf("mcl_grayscale finished\n");
    printf("60%% complete [============      ]\n");

    setup_cl_context("./rawkernels/pi/pi_cl_kernel.cl");
    test_pi_mcl();
    system("clear");
    printf("mcl_pi finished\n");
    printf("76%% complete [============      ]\n");

    mcl_knn();
    system("clear");
    printf("mcl_knn finished\n");
    printf("85%% complete [==============    ]\n");

    setup_cl_context("./rawkernels/matrix/oclmatmul.cl");
    test_mcl_matrix();
    system("clear");
    printf("mcl_matrix finished\n");
    printf("100%% complete [==================]\n");

}

int main(int argc, char** argv) {

    init_log();
    intro();
#define OCC
#ifdef OCC
    ocl_main();
#endif

#ifndef OCC


    printf("mcl benchmark initialized\n");
    printf("0%% complete [                  ]\n");

    test_stream();
    system("clear");
    printf("mcl_stream finished\n");
    printf("34%% complete [======          ]\n");

    setup_cl_context("./oclkernels/crunch/crunch.cl");
    test_crunch();
    system("clear");
    printf("mcl_stream finished\n");
    printf("50%% complete [========        ]\n");
    //
    //    setup_cl_context("./oclkernels/grayscale/grayscale.cl");
    //    test_grayscale_mcl();
    //    system("clear");
    //    printf("mcl_grayscale finished\n");
    //    printf("60%% complete [============      ]\n");
    //
    //    setup_cl_context("./oclkernels/pi/mclPi.cl");
    //    test_pi_mcl();
    //    system("clear");
    //    printf("mcl_pi finished\n");
    //    printf("76%% complete [============      ]\n");
    //
    //    mcl_knn();
    //    system("clear");
    //    printf("mcl_knn finished\n");
    //    printf("85%% complete [==============    ]\n");
    //
    //    setup_cl_context("./oclkernels/matrix/matrixmultiplication.cl");
    //    test_mcl_matrix();
    //    system("clear");
    //    printf("mcl_matrix finished\n");
    //    printf("100%% complete [==================]\n");

#endif

    clean_cl_variables();

    return (EXIT_SUCCESS);


}

