 
 Device is  Tesla K20m  GPU from  NVIDIA Corporation  with a max of 13 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 160856 microseconds.
   (= 160856 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          105757.4     0.030267     0.030258     0.030314
Scale:         105764.0     0.030265     0.030256     0.030281
Add:           105768.2     0.030273     0.030255     0.030332
Triad:         105680.8     0.030287     0.030280     0.030308
-------------------------------------------------------------
time is 0.152685 sec
pmax = 627739.763784
-------------------------------------------------------------
grayscale time is 0.370053 sec
grayscale Gflops is 1.961232 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.046574 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 43.986204
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006750 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006812 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006811 seconds
-------------------------------------------------------------
Average runtime: 0.006791  GFLOPS: 7951659.407146
 
 Device is  Tesla K20m  GPU from  NVIDIA Corporation  with a max of 13 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 161679 microseconds.
   (= 161679 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          105764.0     0.030259     0.030256     0.030270
Scale:         105764.0     0.030269     0.030256     0.030312
Add:           105764.0     0.030270     0.030256     0.030307
Triad:         105701.6     0.030288     0.030274     0.030372
-------------------------------------------------------------
time is 0.152406 sec
pmax = 628888.720311
-------------------------------------------------------------
grayscale time is 0.370342 sec
grayscale Gflops is 1.959702 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.046220 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 44.319280
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006820 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006816 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.007139 seconds
-------------------------------------------------------------
Average runtime: 0.006925  GFLOPS: 7797893.499816
 
 Device is  Tesla K20m  GPU from  NVIDIA Corporation  with a max of 13 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 160917 microseconds.
   (= 160917 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          105767.4     0.030264     0.030255     0.030298
Scale:         105767.4     0.030278     0.030255     0.030384
Add:           105770.7     0.030260     0.030254     0.030271
Triad:         105701.6     0.030286     0.030274     0.030341
-------------------------------------------------------------
time is 0.152352 sec
pmax = 629112.124858
-------------------------------------------------------------
grayscale time is 0.369817 sec
grayscale Gflops is 1.962484 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.046769 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 44.341333
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006818 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.007185 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006801 seconds
-------------------------------------------------------------
Average runtime: 0.006935  GFLOPS: 7786990.854707
 
 Device is  Tesla K20m  GPU from  NVIDIA Corporation  with a max of 13 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 160481 microseconds.
   (= 160481 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          105760.7     0.030273     0.030257     0.030333
Scale:         105764.0     0.030265     0.030256     0.030286
Add:           105764.0     0.030262     0.030256     0.030286
Triad:         105677.4     0.030286     0.030281     0.030296
-------------------------------------------------------------
time is 0.152831 sec
pmax = 627139.463520
-------------------------------------------------------------
grayscale time is 0.368930 sec
grayscale Gflops is 1.967202 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.046198 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 43.951379
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006822 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006758 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006768 seconds
-------------------------------------------------------------
Average runtime: 0.006783  GFLOPS: 7961535.508817
 
 Device is  Tesla K20m  GPU from  NVIDIA Corporation  with a max of 13 compute units 
work group, work item information
 max loc dim  1024  1024  64 
 Max work group size = 1024
This system uses 8 bytes per array element.
-------------------------------------------------------------
Array size = 50000000 (elements), Offset = 0 (elements)
Memory per array = 381.5 MiB (= 0.4 GiB).
Total memory required = 1144.4 MiB (= 1.1 GiB).
Each kernel will be executed 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
-------------------------------------------------------------
Your clock granularity appears to be less than one microsecond.
Each test below will take on the order of 160219 microseconds.
   (= 160219 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          105757.4     0.030279     0.030258     0.030324
Scale:         105767.4     0.030261     0.030255     0.030272
Add:           105754.0     0.030270     0.030259     0.030303
Triad:         105698.2     0.030304     0.030275     0.030423
-------------------------------------------------------------
time is 0.152626 sec
pmax = 627981.971547
-------------------------------------------------------------
grayscale time is 0.368820 sec
grayscale Gflops is 1.967789 
-------------------------------------------------------------
 10240 work-groups of size 256. 1342177280 Integration steps

The calculation ran in    0.046864 seconds
 pi = 3.141593 for 1342177280 steps
-------------------------------------------------------------
knn time is: 44.516045
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006808 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006823 seconds
-------------------------------------------------------------
Matrix multiplication of dimensions 30000 x 30000 x 30000
Matrix multiplication runtime: 0.006803 seconds
-------------------------------------------------------------
Average runtime: 0.006811  GFLOPS: 7928000.933424
