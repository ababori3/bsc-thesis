
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <sys/time.h>

#ifndef STREAM_ARRAY_SIZE
#define STREAM_ARRAY_SIZE 50000000
#endif

#ifdef NTIMES
#if NTIMES<=1
#define NTIMES 10
#endif
#endif
#ifndef NTIMES
#define NTIMES 10
#endif 


#ifndef OFFSET
#define OFFSET 0
#endif

#define HLINE "-------------------------------------------------------------\n"

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#ifndef STREAM_TYPE
#define STREAM_TYPE double
#endif

#define M 20

static STREAM_TYPE a[STREAM_ARRAY_SIZE + OFFSET],
b[STREAM_ARRAY_SIZE + OFFSET],
c[STREAM_ARRAY_SIZE + OFFSET];

static double avgtime[4] = {0}, maxtime[4] = {0},
mintime[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

static char *label[4] = {"Copy:      ", "Scale:     ",
    "Add:       ", "Triad:     "};

static double bytes[4] = {
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    2 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    3 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE,
    3 * sizeof (STREAM_TYPE) * STREAM_ARRAY_SIZE
};

int quantum, checktick();
int BytesPerWord;
int k;
ssize_t j;
STREAM_TYPE scalar;
double t, times[4][NTIMES];

extern double mysecond();
extern void checkSTREAMresults();

double mysecond() {
    struct timeval tp;
    struct timezone tzp;
    int i;

    i = gettimeofday(&tp, &tzp);
    return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6);
}

void print_results(){
        for (k=1; k<NTIMES; k++) /* note -- skip first iteration */
	{
	for (j=0; j<4; j++)
	    {
	    avgtime[j] = avgtime[j] + times[j][k];
	    mintime[j] = MIN(mintime[j], times[j][k]);
	    maxtime[j] = MAX(maxtime[j], times[j][k]);
	    }
	}
    
    printf("Function    Best Rate MB/s  Avg time     Min time     Max time\n");
    for (j=0; j<4; j++) {
		avgtime[j] = avgtime[j]/(double)(NTIMES-1);

		printf("%s%12.1f  %11.6f  %11.6f  %11.6f\n", label[j],
	       1.0E-06 * bytes[j]/mintime[j],
	       avgtime[j],
	       mintime[j],
	       maxtime[j]);
    }
    printf(HLINE);
}

void test_stream() {
    for (k = 0; k < NTIMES; k++) {
        
        times[0][k] = mysecond();
        
        for (j = 0; j < STREAM_ARRAY_SIZE; j++)
            c[j] = a[j];
        times[0][k] = mysecond() - times[0][k];

        times[1][k] = mysecond();
        
        for (j = 0; j < STREAM_ARRAY_SIZE; j++)
            b[j] = scalar * c[j];
        times[1][k] = mysecond() - times[1][k];

        times[2][k] = mysecond();
        
        for (j = 0; j < STREAM_ARRAY_SIZE; j++)
            c[j] = a[j] + b[j];
        times[2][k] = mysecond() - times[2][k];

        times[3][k] = mysecond();
       
        for (j = 0; j < STREAM_ARRAY_SIZE; j++)
            a[j] = b[j] + scalar * c[j];
        times[3][k] = mysecond() - times[3][k];
    }

}

void get_test_times(int t, int quantum) {
    t = mysecond();
    for (int j = 0; j < STREAM_ARRAY_SIZE; j++) a[j] = 2.0E0 * a[j];
    t = 1.0E6 * (mysecond() - t);

    printf("Each test below will take on the order"
            " of %d microseconds.\n", (int) t);
    printf("   (= %d clock ticks)\n", (int) (t / quantum));
    printf("Increase the size of the arrays if this shows that\n");
    printf("you are not getting at least 20 clock ticks per test.\n");

    printf(HLINE);

    printf("WARNING -- The above is only a rough guideline.\n");
    printf("For best results, please be sure you know the\n");
    printf("precision of your system timer.\n");
    printf(HLINE);
}

int checktick() {
    int i, minDelta, Delta;
    double t1, t2, timesfound[M];

    /*  Collect a sequence of M unique time values from the system. */

    for (i = 0; i < M; i++) {
        t1 = mysecond();
        while (((t2 = mysecond()) - t1) < 1.0E-6)
            ;
        timesfound[i] = t1 = t2;
    }

    /*
     * Determine the minimum difference between these M values.
     * This result will be our estimate (in microseconds) for the
     * clock granularity.
     */

    minDelta = 1000000;
    for (i = 1; i < M; i++) {
        Delta = (int) (1.0E6 * (timesfound[i] - timesfound[i - 1]));
        minDelta = MIN(minDelta, MAX(Delta, 0));
    }

    return (minDelta);
}

int check_granularity(int j, int quantum) {

    for (j = 0; j < STREAM_ARRAY_SIZE; j++) {
        a[j] = 1.0;
        b[j] = 2.0;
        c[j] = 0.0;
    }

    printf(HLINE);

    if ((quantum = checktick()) >= 1) {
        printf("Your clock granularity/precision appears to be " "%d microseconds.\n", quantum);
    } else {
        printf("Your clock granularity appears to be "
                "less than one microsecond.\n");
        quantum = 1;
    }
    return quantum;
}

void intro() {
    /* --- SETUP --- determine precision and check timing --- */
    int BytesPerWord;

    printf(HLINE);
    printf("STREAM version $Revision: 5.10 $\n");
    printf(HLINE);
    BytesPerWord = sizeof (STREAM_TYPE);
    printf("This system uses %d bytes per array element.\n",
            BytesPerWord);

    printf(HLINE);
#ifdef N
    printf("*****  WARNING: ******\n");
    printf("      It appears that you set the preprocessor variable N when compiling this code.\n");
    printf("      This version of the code uses the preprocesor variable STREAM_ARRAY_SIZE to control the array size\n");
    printf("      Reverting to default value of STREAM_ARRAY_SIZE=%llu\n", (unsigned long long) STREAM_ARRAY_SIZE);
    printf("*****  WARNING: ******\n");
#endif

    printf("Array size = %llu (elements), Offset = %d (elements)\n", (unsigned long long) STREAM_ARRAY_SIZE, OFFSET);
    printf("Memory per array = %.1f MiB (= %.1f GiB).\n",
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0),
            BytesPerWord * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.0 / 1024.0));
    printf("Total memory required = %.1f MiB (= %.1f GiB).\n",
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024.),
            (3.0 * BytesPerWord) * ((double) STREAM_ARRAY_SIZE / 1024.0 / 1024. / 1024.));
    printf("Each kernel will be executed %d times.\n", NTIMES);
    printf(" The *best* time for each kernel (excluding the first iteration)\n");
    printf(" will be used to compute the reported bandwidth.\n");
}

int main(int argc, char** argv) {

    intro();
    quantum = check_granularity(j, quantum);
    get_test_times(t, quantum);
    test_stream();
    print_results();
    


    //    printf("Function    Best Rate MB/s  Avg time     Min time     Max time\n");
    //    for (j=0; j<4; j++) {
    //		avgtime[j] = avgtime[j]/(double)(NTIMES-1);
    //
    //		printf("%s%12.1f  %11.6f  %11.6f  %11.6f\n", label[j],
    //	       1.0E-06 * bytes[j]/mintime[j],
    //	       avgtime[j],
    //	       mintime[j],
    //	       maxtime[j]);
    //    }
    //    printf(HLINE);

    return (EXIT_SUCCESS);
}



